This repository is a prepared DDEV environment, that results in a running Proof-of-Concept of a Drupal site with a MongoDB backend. No relational database used. The site runs on Drupal 8.6.7 with PHP 7.2 and MongoDB 4.2. Again this repository is only a Proof-of-Concept, **DO NOT USE FOR PRODUCTION!**

Howto get this environment running:
1. Have the DDEV tool, for your local developer environment, running on your local machine. For more information see: [DDEV Documentation](https://ddev.readthedocs.io/en/stable/)
2. Git clone this repository somewhere on your local machine and do not change default installation directory. It should be `mongodb867`.
3. Change the current directory by running: `cd mongodb867`.
4. Start the prepared DDEV environment by running: `ddev start`. Running this command should create the file `mongodb867/web/sites/default/settings.ddev.php`.
5. In the repository change in the file `mongodb867/web/sites/default/settings.ddev.php` the following code from:
   ```
   $databases['default']['default'] = array(
     'database' => "db",
     'username' => "db",
     'password' => "db",
     'host' => $host,
     'driver' => $driver,
     'port' => $port,
     'prefix' => "",
   );
   ```
   to:
   ```
   $databases['default']['default'] = array(
     'database' => 'db',
     'username' => 'db',
     'password' => 'db',
     'prefix' => '',
     'host' => 'ddev-mongodb867-mongo',
     'port' => '27017',
     'namespace' => 'Drupal\\mongodb\\Driver',
     'driver' => 'mongodb',
   );
   ```

6. Get the link to mongo-express, the MongoDB web administrator site that is included in this repository, run the command: `ddev describe`. It wil be something like: http://mongodb867.ddev.site:8081/.
7. Open the mongo-express administrator site and create a new database named `db`. It is on the top right of the administrator site.
8. After that open in a browser the site http://mongodb867.ddev.site/ and install Drupal in the regular way with the standard profile.
9. After the installation process has completed. You will be confronted with the error message "The website encountered an unexpected error. Please try again later.". Run the command: `ddev drush cr` or go to the page `admin/config/development/performance` and click on the button "Clear all caches".
10. Go to the home page of the **Drupal on MongoDB Proof-of-Concept** site and you should see the regular page for an empty Drupal site.
11. If you would like to keep the created website and not have to recreate it the next time you run `ddev start`, than add the setting `disable_settings_management: true` to the file `mongodb867/.ddev/config.yaml`.
12. To stop the website and its DDEV environment, run the command `ddev stop`.

In the mongo-express administrator site you can in the database `db` all regular base tables of Drupal. No related data, translation or field tables. All that data is stored in the MongoDB document in the base table. To get an idea how data is stored open the `users` table and click on user #1.It should look like: ![User #1 in the MongoDB](user1.png)








