<?php

namespace Drupal\mongodb\Path;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Path\AliasStorage as CoreAliasStorage;
use Drupal\mongodb\Driver\Condition;
use Drupal\mongodb\Driver\Statement;

/**
 * The MongoDB implementation of \Drupal\Core\Path\AliasStorage.
 */
class AliasStorage extends CoreAliasStorage {

  /**
   * Indicator for the existence of the database table.
   *
   * @var bool
   */
   protected $tableExists = FALSE;

  /**
   * {@inheritdoc}
   */
  public function save($source, $alias, $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED, $pid = NULL) {
    // For MongoDB the table need to exists. Otherwise MongoDB creates one
    // without the correct validation.
    if (!$this->tableExists) {
      $this->tableExists = $this->ensureTableExists();
    }

    if ($source[0] !== '/') {
      throw new \InvalidArgumentException(sprintf('Source path %s has to start with a slash.', $source));
    }

    if ($alias[0] !== '/') {
      throw new \InvalidArgumentException(sprintf('Alias path %s has to start with a slash.', $alias));
    }

    $fields = [
      'source' => $source,
      'alias' => $alias,
      'langcode' => $langcode,
    ];

    // Insert or update the alias.
    if (empty($pid)) {
      $query = $this->connection->insert(static::TABLE)
        ->fields($fields);
      $fields['pid'] = $pid = (int) $query->execute();
      $operation = 'insert';
    }
    else {
      $prefixed_table = $this->connection->getMongodbPrefixedTable(static::TABLE);
      $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
        ['pid' => (int) $pid],
        ['projection' => ['source' => 1, 'alias' => 1, 'langcode' => 1, '_id' => 0]]
      );

      $statement = new Statement($this->connection, $cursor, ['source', 'alias', 'langcode']);
      $original = $statement->execute()->fetchAssoc();
      if (!$original) {
        $original = FALSE;
      }

      $fields['pid'] = (int) $pid;
      $query = $this->connection->update(static::TABLE)
        ->fields($fields)
        ->condition('pid', (int) $pid);
      $pid = (int) $query->execute();
      $fields['original'] = $original;
      $operation = 'update';
    }
    if ($pid) {
      // @todo Switch to using an event for this instead of a hook.
      $this->moduleHandler->invokeAll('path_' . $operation, [$fields]);
      Cache::invalidateTags(['route_match']);
      return $fields;
    }
    return FALSE;

  }

  /**
   * {@inheritdoc}
   */
  public function load($conditions) {
    // For MongoDB the table need to exists. Otherwise MongoDB creates one
    // without the correct validation.
    if (!$this->tableExists) {
      $this->tableExists = $this->ensureTableExists();
    }

    if (isset($conditions['pid'])) {
      $conditions['pid'] = (int) $conditions['pid'];
    }

    return parent::load($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($conditions) {
    foreach ($conditions as $field => &$value) {
      if ($field == 'pid') {
        $value = (int) $value;
      }
    }

    return parent::delete($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function preloadPathAlias($preloaded, $langcode) {
    $langcode_list = [$langcode, LanguageInterface::LANGCODE_NOT_SPECIFIED];
    $select = $this->connection->select(static::TABLE)
      ->fields(static::TABLE, ['source', 'alias']);

    if (!empty($preloaded)) {
      $conditions = new Condition('OR');
      foreach ($preloaded as $preloaded_item) {
        $conditions->condition('source', $this->connection->escapeLike($preloaded_item), 'LIKE');
      }
      $select->condition($conditions);
    }

    // Always get the language-specific alias before the language-neutral one.
    // For example 'de' is less than 'und' so the order needs to be ASC, while
    // 'xx-lolspeak' is more than 'und' so the order needs to be DESC. We also
    // order by pid ASC so that fetchAllKeyed() returns the most recently
    // created alias for each source. Subsequent queries using fetchField() must
    // use pid DESC to have the same effect.
    if ($langcode == LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      array_pop($langcode_list);
    }
    elseif ($langcode < LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      $select->orderBy('langcode', 'ASC');
    }
    else {
      $select->orderBy('langcode', 'DESC');
    }

    $select->orderBy('pid', 'ASC');
    $select->condition('langcode', $langcode_list, 'IN');
    try {
      return $select->execute()->fetchAllKeyed();
    }
    catch (\Exception $e) {
      $this->catchException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function aliasExists($alias, $langcode, $source = NULL) {
    // Use LIKE and NOT LIKE for case-insensitive matching.
    $query = $this->connection->select(static::TABLE)
      ->fields(static::TABLE, ['pid'])
      ->condition('alias', $this->connection->escapeLike($alias), 'LIKE');
    if (!empty($langcode)) {
      $query->condition('langcode', $langcode);
    }
    if (!empty($source)) {
      $query->condition('source', $this->connection->escapeLike($source), 'NOT LIKE');
    }
    $query->range(0, 1);
    try {
      return (bool) $query->execute()->fetchField();
    }
    catch (\Exception $e) {
      $this->catchException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function languageAliasExists() {
    try {
      return (bool) $this->connection->select('url_alias', 'u')
        ->fields('u', ['pid'])
        ->condition('langcode', LanguageInterface::LANGCODE_NOT_SPECIFIED, '<>')
        ->range(0, 1)
        ->execute()
        ->fetchField();
    }
    catch (\Exception $e) {
      $this->catchException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function pathHasMatchingAlias($initial_substring) {
    $query = $this->connection->select(static::TABLE, 'u')
      ->fields('u', ['pid']);
    try {
      return (bool) $query
        ->condition('source', $this->connection->escapeLike($initial_substring) . '%', 'LIKE')
        ->range(0, 1)
        ->execute()
        ->fetchField();
    }
    catch (\Exception $e) {
      $this->catchException($e);
      return FALSE;
    }
  }

}
