<?php

namespace Drupal\mongodb\EntityStorage;

use Drupal\aggregator\ItemStorageInterface;
use Drupal\aggregator\ItemStorageTrait;

/**
 * The MongoDB implementation of \Drupal\aggregator\ItemStorage.
 */
class ItemStorage extends ContentEntityStorage implements ItemStorageInterface {

  use ItemStorageTrait;

}
