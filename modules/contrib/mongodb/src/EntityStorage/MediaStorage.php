<?php

namespace Drupal\mongodb\EntityStorage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\media\MediaStorageTrait;

/**
 * The MongoDB implementation of \Drupal\media\MediaStorage.
 */
class MediaStorage extends ContentEntityStorage {

  use MediaStorageTrait;

}
