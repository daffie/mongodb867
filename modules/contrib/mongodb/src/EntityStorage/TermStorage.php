<?php

namespace Drupal\mongodb\EntityStorage;

use Drupal\taxonomy\TermStorageInterface;
use Drupal\taxonomy\TermStorageTrait;

/**
 * The MongoDB implementation of \Drupal\taxonomy\TermStorage.
 */
class TermStorage extends ContentEntityStorage implements TermStorageInterface {

  use TermStorageTrait;

  /**
   * {@inheritdoc}
   */
  protected function loadTreeData($vid) {
    $this->treeChildren[$vid] = [];
    $this->treeParents[$vid] = [];
    $this->treeTerms[$vid] = [];
    $query = $this->database->select($this->getBaseTable(), 't');
    $result = $query
      ->addTag('taxonomy_term_access')
      ->fields('t', ['tid', 'taxonomy_term_translations'])
      ->condition('taxonomy_term_translations.vid', $vid)
      ->condition('taxonomy_term_translations.default_langcode', TRUE)
      ->orderBy('taxonomy_term_translations.weight')
      ->orderBy('taxonomy_term_translations.name')
      ->execute();
    foreach ($result as $term) {
      $parents = [];
      if (isset($term->taxonomy_term_translations)) {
        foreach ($term->taxonomy_term_translations as $taxonomy_term_translation) {
          if (isset($taxonomy_term_translation['default_langcode']) && ($taxonomy_term_translation['default_langcode'] == 1)) {
            $term->default_langcode = $taxonomy_term_translation['default_langcode'];
            if (isset($taxonomy_term_translation['tid'])) {
              $term->tid = $taxonomy_term_translation['tid'];
            }
            if (isset($taxonomy_term_translation['vid'])) {
              $term->vid = $taxonomy_term_translation['vid'];
            }
            if (isset($taxonomy_term_translation['uuid'])) {
              $term->uuid = $taxonomy_term_translation['uuid'];
            }
            if (isset($taxonomy_term_translation['langcode'])) {
              $term->langcode = $taxonomy_term_translation['langcode'];
            }
            if (isset($taxonomy_term_translation['name'])) {
              $term->name = $taxonomy_term_translation['name'];
            }
            if (isset($taxonomy_term_translation['weight'])) {
              $term->weight = $taxonomy_term_translation['weight'];
            }
            if (isset($taxonomy_term_translation['changed'])) {
              $term->changed = $taxonomy_term_translation['changed'];
            }
            if (isset($taxonomy_term_translation['taxonomy_term_translations__parent']) && is_array($taxonomy_term_translation['taxonomy_term_translations__parent'])) {
              foreach ($taxonomy_term_translation['taxonomy_term_translations__parent'] as $taxonomy_term_translation__parent) {
                if (isset($taxonomy_term_translation__parent['parent_target_id'])) {
                  $parents[] = $taxonomy_term_translation__parent['parent_target_id'];
                }
              }
              $term->taxonomy_term_translations__parent = $taxonomy_term_translation['taxonomy_term_translations__parent'];
            }
            else {
              $parents[] = 0;
            }
          }
        }
        unset($term->taxonomy_term_translations);
      }
      foreach ($parents as $term_parent) {
        $this->treeChildren[$vid][$term_parent][] = $term->tid;
        $this->treeParents[$vid][$term->tid][] = $term_parent;
      }
      $this->treeTerms[$vid][$term->tid] = $term;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function nodeCount($vid) {
    // TODO: There is too little testing for this. Why is there a join in this
    // query. See: \Drupal\Tests\taxonomy\Functional\TokenReplaceTest.
    $query = $this->database->select('taxonomy_index', 'ti');
    $query->addMongodbJoin('LEFT', 'taxonomy_term_data', 'tid', 'taxonomy_index', 'tid', '=', 'td', [['field' => 'taxonomy_term_translations.vid', 'value' => $vid]]);
    $query->addTag('vocabulary_node_count');
    $results = $query->execute()->fetchAll();
    $nids = [];
    foreach ($results as $result) {
      if (isset($result->nid) && !in_array($result->nid, $nids)) {
        $nids[] = $result->nid;
      }
    }
    return count($nids);
  }

  /**
   * {@inheritdoc}
   */
  public function resetWeights($vid) {
    $prefixed_table = $this->database->getMongodbPrefixedTable('taxonomy_term_data');
    $result = $this->database->getConnection()->{$prefixed_table}->updateMany(
      [
        'vid' => $vid
      ],
      [ '$set' => [
        'weight' => 0,
        "taxonomy_term_translations.$[translation].weight" => 0
      ]],
      [ 'arrayFilters' => [
        [ "translation.vid" => $vid]
      ]]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeTerms(array $nids, array $vocabs = [], $langcode = NULL) {
    $query = db_select('taxonomy_term_data', 'td');
    foreach ($nids as &$nid) {
      $nid = (int) $nid;
    }
    $extra = [[
      'field' => 'nid',
      'value' => $nids,
      'operator' => 'IN'
    ]];
    $query->addMongodbJoin('INNER', 'taxonomy_index', 'tid', 'taxonomy_term_data', 'tid', '=', 'tn', $extra);
    $query->fields('td', ['tid']);
    $query->addField('tn', 'nid', 'node_nid');
    $query->orderby('taxonomy_term_translations.weight');
    $query->orderby('taxonomy_term_translations.name');
    $query->addTag('taxonomy_term_access');
    if (!empty($vocabs)) {
      $query->condition('taxonomy_term_translations.vid', $vocabs, 'IN');
    }
    if (!empty($langcode)) {
      $query->condition('taxonomy_term_translations.langcode', $langcode);
    }

    $results = [];
    $all_tids = [];
    foreach ($query->execute() as $term_record) {
      if (isset($term_record->tn_node_nid) && is_array($term_record->tn_node_nid)) {
        foreach ($term_record->tn_node_nid as $node_nid) {
          $results[$node_nid][] = $term_record->tid;
        }
      }
      $all_tids[] = $term_record->tid;
    }

    $all_terms = $this->loadMultiple($all_tids);
    $terms = [];
    foreach ($results as $nid => $tids) {
      foreach ($tids as $tid) {
        $terms[$nid][$tid] = $all_terms[$tid];
      }
    }
    return $terms;
  }

}
