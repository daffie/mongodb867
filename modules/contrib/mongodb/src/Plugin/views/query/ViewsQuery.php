<?php

namespace Drupal\mongodb\Plugin\views\query;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\mongodb\Driver\Condition;
use Drupal\mongodb\Driver\MongodbSQLException;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\LatestRevision;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Views query plugin for an MongoDB query.
 *
 * @ingroup views_query_plugins
 *
 * @ViewsQuery(
 *   id = "mongodb_views_query",
 *   title = @Translation("MongoDB Query"),
 *   help = @Translation("Query will be generated and run using the Drupal database API.")
 * )
 */
class ViewsQuery extends Sql {

  /**
   * An array of sections of the condition part of the query. Works the same
   * variable $where, with the exception that it can also contain full Condition
   * objects.
   */
  public $condition = [];

  /**
   * An array of sections of the having condition part of the query. Works the
   * same variable $having, with the exception that it can also contain full
   * Condition objects.
   */
  public $havingCondition = [];

  /**
   * A list of embedded table paths for the query to unwind before the $match
   * part of the query.
   *
   * @var array
   */
  protected $mongodbFilterUnwindPaths = [];

  /**
   * The array containing the fields to added for conditions to the query.
   *
   * @var array
   */
  protected $mongodbConditionFields = [];

  /**
   * The array containing the fields to be concatenated to the query.
   *
   * @var array
   */
  protected $mongodbConcatFields = [];

  /**
   * The array containing the fields to be substringed to the query.
   *
   * @var array
   */
  protected $mongodbSubstringFields = [];

  /**
   * The array containing the fields to be multiplied and sumed to the query.
   *
   * @var array
   */
  protected $mongodbSumMultiplyFields = [];

  /**
   * The array containing the fields with their length to be added to the query.
   *
   * @var array
   */
  protected $mongodbFieldsLength = [];

  /**
   * The array containing the date fields as a date string be added to the query.
   *
   * @var array
   */
  protected $mongodbDateDateFormattedFields = [];

  /**
   * The array containing the string fields as a date string be added to the query.
   *
   * @var array
   */
  protected $mongodbDateStringFormattedFields = [];

  /**
   * The array containing the group by operation for the query.
   *
   * @var array
   */
  protected $mongodbGroupByOperation = [];

  /**
   * The array containing the unrelated joins for the query.
   *
   * @var array
   */
  protected $mongodbUnrelatedJoins = [];

  /**
   * Get the current revision table from the view.
   *
   * @return
   *   The current revision table.
   */
  public function getCurrentRevisionTable() {
    return $this->view->storage->get('current_revision_table');
  }

  /**
   * Get the all revisions table from the view.
   *
   * @return
   *   The all revisions table.
   */
  public function getAllRevisionsTable() {
    return $this->view->storage->get('all_revisions_table');
  }

  /**
   * Get the latest revision table from the view.
   *
   * @return
   *   The latest revision table.
   */
  public function getLatestRevisionTable() {
    return $this->view->storage->get('latest_revision_table');
  }

  /**
   * Join against another table in the database.
   *
   * @param $type
   *   The type of join. This can be INNER, LEFT (OUTER). Defaults to LEFT.
   * @param $table
   *   The table against which to join. May be a string or another SelectQuery
   *   object. If a query object is passed, it will be used as a subselect.
   *   Unless the table name starts with the database / schema name and a dot
   *   it will be prefixed.
   * @param $field
   *   The field of the table $table to use in the join.
   * @param $left_table
   *   The table to which the table $table joins to.
   * @param $left_field
   *   The field of the table $left_table to use in the join.
   * @param $operator
   *   The operator to use in the join. Defaults to '='.
   * @param $alias
   *   The alias for the table. In most cases this should be the first letter
   *   of the table, or the first letter of each "word" in the table. If omitted,
   *   one will be dynamically generated.
   * @param $extra
   *   An array of extra conditions on the join.
   *
   * @return
   *   The unique alias that was assigned for this table.
   *
   * @see \Drupal\mongodb\Driver\Select::addMongodbJoin()
   */
  public function addUnrelatedJoin($type, $table, $field, $left_table, $left_field, $operator = '=', $alias = NULL, $extra = []) {
    if (empty($alias)) {
      $alias = $table . '_' . str_replace('.', '_', $field);
    }

    $this->mongodbUnrelatedJoins[] = [
      'type' => $type,
      'table' => $table,
      'field' => $field,
      'left_table' => $left_table,
      'left_field' => $left_field,
      'operator' => $operator,
      'alias' => $alias,
      'extra' => $extra
    ];

    return $alias;
  }

  /**
   * {@inheritdoc}
   */
  protected function truncateAlias($alias) {
    return $alias;
  }

  /**
   * {@inheritdoc}
   */
  protected function removeEmbeddedTableDataForNonRelationalDatabases($field) {
    $last_dot_position = strrpos($field, '.');
    if ($last_dot_position !== FALSE) {
      $field = substr($field, ($last_dot_position + 1));
    }
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    throw new MongodbSQLException('MongoDB does not support SQL strings. Please use the method ViewsQuery::addCondition() instead of this method.');
  }

  /**
   * {@inheritdoc}
   */
  public function addWhereExpression($group, $snippet, $args = []) {
    throw new MongodbSQLException('MongoDB does not support SQL strings. Please use the method ViewsQuery::addCondition() instead of this method.');
  }

  /**
   * Path to be unwound before the condition.
   *
   * @param string $path
   *   The path of the embedded table to be unwound.
   */
  public function addFilterUnwindPath($path) {
    $this->mongodbFilterUnwindPaths[] = $path;
  }

  /**
   * Add a field to the query that can be used by a condition.
   *
   * @param string $alias
   *   The alias to be used to store the result in.
   * @param string $field
   *   The name of the field hows value is assigned to the alias.
   */
  public function addConditionField($alias, $field) {
    $this->mongodbConditionFields[$alias] = $field;
  }

  /**
   * Add a field to the query that holds the result of the concatination.
   *
   * @param string $alias
   *   The alias to be used to store the result in.
   * @param array $fields
   *   The list of fields for the concatination.
   * @param array $integer_fields
   *   The list integer fields for the concatination.
   */
  public function addConcatField($alias, array $fields = [], array $integer_fields = []) {
    $this->mongodbConcatFields[$alias] = [
      'fields' => $fields,
      'alias' => $alias,
      'integer fields' => $integer_fields
    ];
  }

  /**
   * Add a field to the query that holds the result of the field length.
   *
   * @param string $alias
   *   The alias to be used to store the result in.
   * @param string $field
   *   The name of the field for which to get its length.
   */
  public function addFieldLength($alias, $field) {
    $this->mongodbFieldsLength[$alias] = $field;
  }

  /**
   * Add a field to the query with a date string value.
   *
   * @param string $alias
   *   The alias to be used to store the result in.
   * @param string $field
   *   The name of the field for the basis of the date string. The field must be
   *   a date field.
   * @param string $format
   *   The format used to create the date string.
   */
  public function addDateDateFormattedField($alias, $field, $format) {
    $last_dot = strrpos($field, '.');
    if ($last_dot !== FALSE) {
      $this->addFilterUnwindPath(substr($field, 0, $last_dot));
    }

    $this->mongodbDateDateFormattedFields[$alias] = [
      'field' => $field,
      'alias' => $alias,
      'format' => $format
    ];
  }

  /**
   * Add a field to the query with a date string value.
   *
   * @param string $alias
   *   The alias to be used to store the result in.
   * @param string $field
   *   The name of the field for the basis of the date string. The field must be
   *   a string field with a date value.
   * @param string $format
   *   The format used to create the date string.
   */
  public function addDateStringFormattedField($alias, $field, $format) {
    $last_dot = strrpos($field, '.');
    if ($last_dot !== FALSE) {
      $this->addFilterUnwindPath(substr($field, 0, $last_dot));
    }

    $this->mongodbDateStringFormattedFields[$alias] = [
      'field' => $field,
      'alias' => $alias,
      'format' => $format
    ];
  }

  /**
   * Add a field to the query that holds the result of substring value.
   *
   * @param string $alias
   *   The alias to be used to store the result in.
   * @param string $field
   *   The name of the field for substring value.
   * @param int $start
   *   The start position for the substring value.
   * @param int $start
   *   The length value for the substring.
   */
  public function addSubstringField($alias, $field, $start, $length) {
    $this->mongodbSubstringFields[$alias] = [
      'alias' => $alias,
      'field' => $field,
      'start' => $start,
      'length' => $length,
    ];
  }

  /**
   * Add a field to the query with the sum of the multiplications.
   *
   * @param string $alias
   *   The alias to be used to store the result in.
   * @param array $fields
   *   The list of fields to multiply.
   * @param array $values
   *   The list of values to multiply by.
   */
  public function addSumMultiplyExpression($alias, $fields, $values) {
    $this->mongodbSumMultiplyFields[$alias] = [
      'alias' => $alias,
      'fields' => $fields,
      'values' => $values,
    ];
  }

  /**
   * Add a condition clause to the query.
   *
   * @param $group
   *   The WHERE group to add these to; groups are used to create AND/OR
   *   sections. Groups cannot be nested. Use 0 as the default group.
   *   If the group does not yet exist it will be created as an AND group.
   * @param $field
   *   The name of the field to check.
   * @param $value
   *   The value to test the field against. In most cases, this is a scalar. For
   *   more complex options, it is an array. The meaning of each element in the
   *   array is dependent on the $operator.
   * @param $operator
   *   The comparison operator, such as =, <, or >=. It also accepts more
   *   complex options such as IN, LIKE, LIKE BINARY, or BETWEEN. Defaults to =.
   *
   * @see \Drupal\views\Plugin\views\query\Sql::addWhere()
   */
  public function addCondition($group, $field, $value = NULL, $operator = '=') {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->condition[$group])) {
      $this->setWhereGroup('AND', $group, 'condition');
    }

    if ($field instanceof Condition) {
      $this->condition[$group]['conditions'][] = $field;
    }
    else {
      $substitutions = \Drupal::moduleHandler()->invokeAll('views_query_substitutions', [$this->view]);
      if (is_array($value)) {
        foreach ($value as &$val) {
          foreach ($substitutions as $substitution_key => $substitution_value) {
            if ($substitution_key === $val) {
              $val = $substitution_value;
            }
          }
        }
      }
      else {
        foreach ($substitutions as $substitution_key => $substitution_value) {
          if ($substitution_key === $value) {
            $value = $substitution_value;
          }
        }
      }

      $this->condition[$group]['conditions'][] = [
        'field' => $field,
        'value' => $value,
        'operator' => $operator,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addHavingExpression($group, $snippet, $args = []) {
    throw new MongodbSQLException('MongoDB does not support SQL strings. Please use the method ViewsQuery::addHavingCondition() instead of this method.');
  }

  /**
   * Add a HAVING condition clause to the query.
   *
   * @param $group
   *   The HAVING group to add these to; groups are used to create AND/OR
   *   sections. Groups cannot be nested. Use 0 as the default group.
   *   If the group does not yet exist it will be created as an AND group.
   * @param $field
   *   The name of the field to check.
   * @param $value
   *   The value to test the field against. In most cases, this is a scalar. For
   *   more complex options, it is an array. The meaning of each element in the
   *   array is dependent on the $operator.
   * @param $operator
   *   The comparison operator, such as =, <, or >=. It also accepts more
   *   complex options such as IN, LIKE, LIKE BINARY, or BETWEEN. Defaults to =.
   *   If $field is a string you have to use 'formula' here.
   *
   * @see \Drupal\Core\Database\Query\ConditionInterface::condition()
   * @see \Drupal\Core\Database\Query\Condition
   */
  public function addHavingCondition($group, $field, $value = NULL, $operator = '=') {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->havingCondition[$group])) {
      $this->setWhereGroup('AND', $group, 'havingCondition');
    }

    if ($field instanceof Condition) {
      $this->havingCondition[$group]['conditions'][] = $field;
    }
    else {
      $this->havingCondition[$group]['conditions'][] = [
        'field' => $field,
        'value' => $value,
        'operator' => $operator,
      ];
    }
  }

  /**
   * Add a group by operation to the query.
   *
   * @param string $alias
   *   The alias to be used to store the result on the operation in.
   * @param string $field
   *   The name of the field that is used.
   * @param string $operator
   *   The group by operation that used on the field.
   */
  public function addGroupByOperation($alias, $field, $operator) {
    if (!empty($alias) && !empty($field) && !empty($operator)) {
      $this->mongodbGroupByOperation[$alias] = [
        'field' => $field,
        'alias' => $alias,
        'operator' => $operator
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function removeCurrentRevisionTableForNonRelationalDatabases($field) {
    if (!empty($this->getCurrentRevisionTable())) {
      $search_needle = $this->getCurrentRevisionTable() . '.';
      $field = str_replace($search_needle, '', $field);
    }
    return $field;
  }

  /**
   * Generates a unique placeholder used in the db query.
   */
  public function placeholder($base = 'views') {
    static $placeholders = [];
    if (!isset($placeholders[$base])) {
      $placeholders[$base] = 0;
      return $base;
    }
    else {
      return $base . ++$placeholders[$base];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCondition($where = 'where') {
    foreach ($this->$where as $group => &$info) {
      if (!empty($info['conditions'])) {
        foreach ($info['conditions'] as &$clause) {
          if (is_array($clause) && isset($clause['operator']) && !in_array($clause['operator'], ['DATEDATE', 'DATESTRING']) &&  isset($clause['value'])) {
            if (is_int($clause['value']) || ctype_digit($clause['value'])) {
              $clause['value'] = (int) $clause['value'];
            }
            elseif (is_array($clause['value'])) {
              foreach($clause['value'] as &$clause_value) {
                if (is_int($clause_value) || ctype_digit($clause_value)) {
                  $clause_value = (int) $clause_value;
                }
              }
            }
          }
        }
      }
    }

    return parent::buildCondition($where);
  }

  /**
   * {@inheritdoc}
   */
  protected function compileFieldsNotEmptyFieldFunction($query, $field, $fieldname, $fieldalias) {
    $query->addGroupByOperation($fieldalias, $fieldname, strtoupper($field['function']));
    $this->hasAggregate = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function compileFieldsEmptyFieldTable($query, $field, $fieldname, $fieldalias) {
    $query->addGroupField($fieldalias, $fieldname);
  }

  public function hasLatestRevisionFilter() {
    foreach ($this->view->filter as $handler) {
      if ($handler instanceof LatestRevision) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFieldsForNonRelationalDatabases(&$query) {
    // Do this if the base table is a revisional table.
    if ($this->view->storage->get('mongodb_base_table') != $this->view->storage->get('original_base_table')) {
      if (!empty($this->getAllRevisionsTable()) && !empty($this->getLatestRevisionTable())) {
        if ($this->hasLatestRevisionFilter()) {
          $query->embeddedTableToUseAsBaseTable($this->getLatestRevisionTable());
        }
        else {
          $query->embeddedTableToUseAsBaseTable($this->getAllRevisionsTable());
        }
      }
      elseif (!empty($this->getCurrentRevisionTable())) {
        $query->embeddedTableToUseAsBaseTable($this->getCurrentRevisionTable());
      }
    }

    foreach ($this->mongodbFilterUnwindPaths as $path) {
      $query->addFilterUnwindPath($path);
    }

    foreach ($this->mongodbConditionFields as $alias => $field) {
      $query->addConditionField($alias, $field);
    }

    foreach ($this->mongodbConcatFields as $alias => $data) {
      $query->addConcatField($alias, $data['fields'], $data['integer fields']);
    }

    foreach ($this->mongodbSubstringFields as $alias => $data) {
      $query->addSubstringField($alias, $data['field'], $data['start'], $data['length']);
    }

    foreach ($this->mongodbFieldsLength as $alias => $field) {
      $query->addFieldLength($alias, $field);
    }

    foreach ($this->mongodbDateDateFormattedFields as $alias => $data) {
      $query->addDateDateFormattedField($alias, $data['field'], $data['format']);
    }

    foreach ($this->mongodbDateStringFormattedFields as $alias => $data) {
      $query->addDateStringFormattedField($alias, $data['field'], $data['format']);
    }
  }


  /**
   * {@inheritdoc}
   */
  protected function addJoins(&$query) {
    // Add all the tables to the query via joins. We assume all LEFT joins.
    foreach ($this->tableQueue as $table) {
      if (is_object($table['join'])) {
        $table['join']->buildMongodbJoin($query, $table, $this);
      }
    }

    foreach ($this->mongodbUnrelatedJoins as $data) {
      $query->addMongodbJoin($data['type'], $data['table'], $data['field'], $data['left_table'], $data['left_field'], $data['operator'], $data['alias'], $data['extra']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function isAggregateQuery() {
    if (count($this->having) || count($this->havingCondition) || count($this->groupby)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function updateBaseFieldForRevisionalForNonRelationalDatabases($base_field, $info) {
    if ($info['revision'] && ($info['relationship_id'] == 'none') && ($this->view->storage->get('mongodb_base_table') != $this->view->storage->get('original_base_table')) && !empty($this->getAllRevisionsTable()) && !empty($this->getLatestRevisionTable())) {
      if ($this->hasLatestRevisionFilter()) {
        $base_field = $this->getLatestRevisionTable() . '.' . $base_field;
      }
      else {
        $base_field = $this->getAllRevisionsTable() . '.' . $base_field;
      }
    }
    return $base_field;
  }

  /**
   * {@inheritdoc}
   */
  protected function updateFieldForGroupByQuery($field) {
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  protected function addGroupByQueryCondition(&$query) {
    if (!empty($this->havingCondition) && $condition = $this->buildCondition('havingCondition')) {
      $query->havingCondition($condition);

      foreach ($this->mongodbGroupByOperation as $operation) {
        $query->addGroupByOperation($operation['alias'], $operation['field'], $operation['operator']);
      }
    }
    foreach ($this->mongodbSumMultiplyFields as $alias => $data) {
      $query->addSumMultiplyExpression($alias, $data['fields'], $data['values']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function addQueryCondition($query) {
    if (!empty($this->condition) && $condition = $this->buildCondition('condition')) {
      $query->condition($condition);

      // Add the unwind actions for the embedded tables for the condition fields.
      foreach ($this->condition as $conditions) {
        if (isset($conditions['conditions']) && is_array($conditions['conditions'])) {
          foreach ($conditions['conditions'] as $condition) {
            if (is_array($condition) && isset($condition['field']) && is_string($condition['field'])) {
              $position = strrpos($condition['field'], '.');
              if ($position !== FALSE) {
                $query->addFilterUnwindPath(substr($condition['field'], 0, $position));
              }
            }
          }
        }
      }
    }

    if (!empty($this->mongodbFilterUnwindPaths)) {
      foreach ($this->mongodbFilterUnwindPaths as $unwind_path) {
        $query->addFilterUnwindPath($unwind_path);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function updateViewsResultForNonRelationalDatabases(&$results) {
    foreach ($results as &$result) {
      foreach ($this->fields as $field) {
        // Change the integer values from MongoDB to the expected string values
        // from relational datbases.
        if (isset($result->{$field['alias']}) && is_int($result->{$field['alias']})) {
          $result->{$field['alias']} = (string) $result->{$field['alias']};
        }

        // Move the join results from their arrayed form from MongoDB to the
        // base result that is expected from ralational databases.
        if (isset($field['table']) && $field['table'] != $this->view->storage->get('base_table') && isset($result->{$field['alias']}) && is_array($result->{$field['alias']})) {
          $result->{$field['alias']} = reset($result->{$field['alias']});
        }

        if (isset($field['table']) && $field['table'] == $this->view->storage->get('base_table') && isset($result->{$field['alias']}) && !isset($result->{$field['field']})) {
          $result->{$field['field']} = $result->{$field['alias']};
        }

        $joined_table_alias = '';
        if (isset($field['alias']) && isset($field['table']) && $field['table'] != $this->view->storage->get('base_table')) {
          $joined_table_alias = $field['table'] . '_' . $field['alias'];
          if (!empty($joined_table_alias) && isset($result->{$joined_table_alias}) && is_array($result->{$joined_table_alias})) {
            $result->{$field['alias']} = reset($result->{$joined_table_alias});
            $result->{$joined_table_alias} = $result->{$field['alias']};
          }
        }
      }
    }
  }

  /**
   * Helper method for changing allowing non relational databases to thansform
   * their views result to match that of relational databases.
   *
   * @param \Drupal\views\ResultRow $result
   *   The result of the SQL query.
   * @param string $id_alias
   *   The key of the entity id to search for.
   * @param array $info
   *   The array with table information.
   */
  protected function getResultIdValueForNonRelationalDatabases($result, $id_alias, $info) {
    $alias_id_alias = $info['alias'] . '_' . $id_alias;
    if (isset($result->{$alias_id_alias}) && $result->{$alias_id_alias} != '') {
      return $result->{$alias_id_alias};
    }
    return NULL;
  }

}
