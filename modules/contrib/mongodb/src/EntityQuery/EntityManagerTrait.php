<?php

namespace Drupal\mongodb\EntityQuery;

/**
 * Provides a trait for getting entity manager service.
 */
trait EntityManagerTrait {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;

  /**
   * Returns the entity manager service.
   *
   * @return \Drupal\Core\Entity\EntityManager
   *   The entity manager service.
   */
  protected function getEntityManager() {
    if (empty($this->entityManager)) {
      $this->entityManager = \Drupal::entityManager();
    }

    return $this->entityManager;
  }

}
