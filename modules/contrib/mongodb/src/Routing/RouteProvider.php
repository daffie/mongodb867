<?php

namespace Drupal\mongodb\Routing;

use Drupal\Core\Routing\RouteProvider as CoreRouteProvider;
use Drupal\mongodb\Driver\Statement;
use Symfony\Component\Routing\RouteCollection;

/**
 * The MongoDB implementation of \Drupal\Core\Routing\RouteProvider.
 */
class RouteProvider extends CoreRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getRoutesByPath($path) {
    $parts = preg_split('@/+@', mb_strtolower($path), NULL, PREG_SPLIT_NO_EMPTY);

    $collection = new RouteCollection();

    $ancestors = $this->getCandidateOutlines($parts);
    if (empty($ancestors)) {
      return $collection;
    }

    try {
      $prefixed_table = $this->connection->getMongodbPrefixedTable($this->tableName);
      $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
        ['pattern_outline' => ['$in' => $ancestors], 'number_parts' => ['$gte' => count($parts)]],
        ['projection' => ['name' => 1, 'route' => 1, 'fit' => 1, '_id' => 0]]
      );

      $statement = new Statement($this->connection, $cursor, ['name', 'route', 'fit']);
      $routes = $statement->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (\Exception $e) {
      $routes = [];
    }

    // We sort by fit and name in PHP to avoid a SQL filesort and avoid any
    // difference in the sorting behavior of SQL back-ends.
    usort($routes, [$this, 'routeProviderRouteCompare']);

    foreach ($routes as $row) {
      $collection->add($row['name'], unserialize($row['route']));
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoutesPaged($offset, $length = NULL) {
    // The problem for MongoDB is that setting the length to the number zero is
    // a special case and is equivalent to setting no length.
    if (isset($length) && $length == 0) {
      return [];
    }

    return parent::getRoutesPaged($offset, $length);
  }

}
