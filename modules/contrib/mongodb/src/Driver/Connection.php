<?php

namespace Drupal\mongodb\Driver;

use Drupal\Core\Database\Connection as DatabaseConnection;
use Drupal\Core\Database\DatabaseNotFoundException;
use Drupal\mongodb\Driver\MongodbSQLException;
use Drupal\mongodb\Service\Sequences;
use Drupal\mongodb\Service\TableInformation;
use MongoDB\Client;
use MongoDB\Database as MongodbDatabase;
use MongoDB\Driver\Exception\AuthenticationException;
use MongoDB\Driver\Exception\ConnectionException;
use MongoDB\Operation\FindOneAndUpdate;

/**
 * MongoDB implementation of \Drupal\Core\Database\Connection.
 */
class Connection extends DatabaseConnection {

  /**
   * Whether this database connection supports transactions.
   *
   * @var bool
   */
  protected $transactionSupport = FALSE;

  /**
   * A map of condition operators to MongoDB operators.
   */
  protected static $mongodbConditionOperatorMap = [
    'IN' => ['mongodb_operator' => '$in'],
    'NOT IN' => ['mongodb_operator' => '$nin'],
    'EXISTS' => ['mongodb_operator' => '$exists'],
    'NOT EXISTS' => ['mongodb_operator' => '$exists'],
    '=' => ['mongodb_operator' => '$eq'],
    '<' => ['mongodb_operator' => '$lt'],
    '>' => ['mongodb_operator' => '$gt'],
    '>=' => ['mongodb_operator' => '$gte'],
    '<=' => ['mongodb_operator' => '$lte'],
    '<>' => ['mongodb_operator' => '$ne'],
    '!=' => ['mongodb_operator' => '$ne'],
    // These ones are here for performance reasons.
    'IS NULL' => [],
    'IS NOT NULL' => [],
    'LIKE' => [],
    'NOT LIKE' => [],
    'BETWEEN' => [],
    'NOT BETWEEN' => [],
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct($connection, array $connection_options) {
    if (!($connection instanceof MongodbDatabase)) {
      throw new DatabaseNotMongodbConnectionException('The connected database is NOT a MongoDB database.');
    }

    // Initialize and prepare the connection prefix.
    $this->setPrefix(isset($connection_options['prefix']) ? $connection_options['prefix'] : '');

    $this->connection = $connection;
    $this->connectionOptions = $connection_options;
  }

  /**
   * Opens a MongoDB\Database connection.
   *
   * @param array $connection_options
   *   The database connection settings array.
   *
   * @return \MonogoDB\Database
   *   A \MonogoDB\Database object.
   */
  public static function open(array &$connection_options = []) {
    // Default to TCP connection on port 27017.
    if (empty($connection_options['port'])) {
      $connection_options['port'] = 27017;
    }

    // If the password contains a backslash it is treated as an escape character
    // http://bugs.php.net/bug.php?id=53217
    // so backslashes in the password need to be doubled up.
    // The bug was reported against pdo_mongodb 1.0.2, backslashes in passwords
    // will break on this doubling up when the bug is fixed, so check the version
    //elseif (phpversion('pdo_mongodb') < 'version_this_was_fixed_in') {
    if (!empty($connection_options['password'])) {
      $connection_options['password'] = str_replace('\\', '\\\\', $connection_options['password']);
    }

    // Default database is test.
    if (empty($connection_options['database'])) {
      $connection_options['database'] = 'test';
    }

    if (!empty($connection_options['username'])) {
      if (!empty($connection_options['password'])) {
        $uri = 'mongodb://' . $connection_options['username'] . ':' . $connection_options['password'] . '@';
      }
      else {
        $uri = 'mongodb://' . $connection_options['username'] . '@';
      }
    }
    else {
      $uri = 'mongodb://';
    }

    if (!empty($connection_options['host'])) {
      $uri .= $connection_options['host'] . ':' . $connection_options['port'];
    }

    try {
      $client = new Client($uri);
      $connection = $client->{$connection_options['database']};
    }
    catch (ConnectionException $e) {
      throw new DatabaseNotFoundException($e->getMessage(), $e->getCode(), $e);
    }
    catch (AuthenticationException $e) {
      throw new DatabaseAccessDeniedException($e->getMessage(), $e->getCode(), $e);
    }

    return $connection;
  }

  /**
   * Returns the database connection object.
   *
   * @return \MongoDB\Database
   *   A object of the database connection.
   */
  public function getConnection() {
    return $this->connection;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare($statement, array $driver_options = []) {
    // MongoDB has no use for this method.
    return $statement;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareQuery($query) {
    return $this->prefixTables($query);
  }

  /**
   * {@inheritdoc}
   */
  public function query($query, array $args = [], $options = []) {
    // Use default values if not already set.
    $options += $this->defaultOptions();

    return \Drupal::service('mongodb.translate_sql')->query($query, $args, $options);
  }

  /**
   * Get a MongoDB prefixed table name.
   *
   * @param string $table
   *   The name of the table in question.
   *
   * @return string
   */
  public function getMongodbPrefixedTable($table) {
    if (strpos($table, '.') !== FALSE) {
      $parts = explode('.', $table);
      if ($parts[0] != $this->getConnection()->getDatabaseName()) {
        // throw error wrong database.
      }
      if (count($parts) > 2) {
        // throw error table name has too many dots.
      }

      // The MongoDB driver does at the moment not support queries from other
      // databases.
      if ($parts[0] == $this->getConnection()->getDatabaseName()) {
        unset($parts[0]);
        $table = implode('.', $parts);
      }

      // A fully qualified table name is already prefixed.
      return $table;
    }

    return $this->tablePrefix($table) . $table;
  }

  /**
   * Get the MongoDB table information service.
   *
   * @return \Drupal\mongodb\Service\TableInformation
   */
  public function tableInformation() {
    return new TableInformation($this);
  }

  /**
   * Get the MongoDB table information service.
   *
   * @return \Drupal\mongodb\Service\Sequences
   */
  public function sequences() {
    return new Sequences($this);
  }

  /**
   * {@inheritdoc}
   */
  public function driver() {
    return 'mongodb';
  }

  /**
   * {@inheritdoc}
   */
  public function version() {
    $cursor = $this->connection->command(['buildInfo' => 1]);
    $build_info = $cursor->toArray()[0];

    return $build_info->version;
  }

  /**
   * {@inheritdoc}
   */
  public function clientVersion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function databaseType() {
    return 'mongodb';
  }

  /**
   * {@inheritdoc}
   */
  public function queryRange($query, $from, $count, array $args = [], array $options = []) {
    return \Drupal::service('mongodb.translate_sql')->queryRange($query, $from, $count, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function queryTemporary($query, array $args = [], array $options = []) {
    $tablename = $this->generateTemporaryTableName();

    $query->createTemporaryTable($tablename);
    $query->execute();

    return $tablename;
  }

  /**
   * {@inheritdoc}
   */
  public function createDatabase($database) {}

  /**
   * {@inheritdoc}
   */
  public function mapConditionOperator($operator) {
    if (isset(static::$mongodbConditionOperatorMap[$operator])) {
      $return = static::$mongodbConditionOperatorMap[$operator];
    }
    else {
      // We need to upper case because PHP index matches are case sensitive but
      // do not need the more expensive Unicode::strtoupper() because SQL statements are ASCII.
      $operator = strtoupper($operator);
      $return = isset(static::$mongodbConditionOperatorMap[$operator]) ? static::$mongodbConditionOperatorMap[$operator] : [];
    }

    $return += ['operator' => $operator];

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function nextId($existing_id = 0) {
    $existing_id = (int) $existing_id;
    $prefixed_table = $this->getMongodbPrefixedTable('sequences');

    // Update the sequence.
    $result = $this->getConnection()->{$prefixed_table}->findOneAndUpdate(
      ['_id' => 1],
      ['$inc' => ['value' => 1]],
      ['returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER]
    );

    if ($result && isset($result->value)) {
      if ($result->value >= $existing_id) {
        return $result->value;
      }
      else {
        // Update the sequence to $existing_id + 1.
        $result = $this->getConnection()->{$prefixed_table}->findOneAndUpdate(
          ['_id' => 1],
          ['$set' => ['value' => $existing_id + 1]],
          ['returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER]
        );

        if ($result && isset($result->value)) {
          return $result->value;
        }
      }
    }
    else {
      $value = $existing_id > 0 ? $existing_id + 1 : 1;
      // Create a new sequence and the sequences table if it does not exists.
      $result = $this->getConnection()->{$prefixed_table}->insertOne([
        '_id' => 1,
        'value' => $value
      ]);

      if ($result && ($result->getInsertedCount() > 0)) {
        return $value;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function pushTransaction($name) {}

  /**
   * {@inheritdoc}
   */
  public function destroy() {
    $this->schema = NULL;
  }

}
