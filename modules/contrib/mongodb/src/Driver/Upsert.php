<?php

namespace Drupal\mongodb\Driver;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Upsert as QueryUpsert;

/**
 * The MongoDB implementation of \Drupal\Core\Database\Query\Upsert.
 */
class Upsert extends QueryUpsert {

  use DocumentInsertTrait;

  /**
   * The MongoDB table information service.
   *
   * @var \Drupal\mongodb\Service\TableInformation
   */
  protected $tableInformation;

  /**
   * {@inheritdoc}
   */
  protected function preExecute() {
    // Do the data validation for the base table.
    $this->validateDataForTableInsert($this->table, $this->insertFields, $this->defaultFields, $this->insertValues);

    // Confirm that the user set the unique/primary key of the table.
    if (!$this->key) {
      throw new NoUniqueFieldException('There is no unique field specified.');
    }

    return isset($this->insertValues[0]) || $this->insertFields;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if (!$this->preExecute()) {
      return NULL;
    }

    $prefixed_table = $this->connection->getMongodbPrefixedTable($this->table);

    $last_insert_id = 0;
    $result = FALSE;

    foreach ($this->insertValues as $insert_values) {
      $insert_document = $this->getInsertDocumentForTable($this->table, $this->insertFields, $insert_values);

      if (isset($insert_document[$this->key])) {
        // Create the update filter and remove the filter key from the
        // insert_document.
        $insert_filter = [];
        $insert_filter[$this->key] = $insert_document[$this->key];
        unset($insert_document[$this->key]);

        $result = $this->connection->getConnection()->{$prefixed_table}->updateOne($insert_filter, ['$set' => $insert_document], ['upsert' => TRUE]);
      }
    }

    // Re-initialize the values array so that we can re-use this query.
    $this->insertValues = [];

    return $last_insert_id;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    // Nothing to do.
  }

}
