<?php

namespace Drupal\mongodb\Driver;

use Drupal\Core\Database\Query\SelectExtender as QuerySelectExtender;

/**
 * The MongoDB implementation of \Drupal\Core\Database\Query\SelectExtender.
 */
class SelectExtender extends QuerySelectExtender {

  /**
   * {@inheritdoc}
   */
  public function conditionGroupFactory($conjunction = 'AND') {
    // Make sure that condition is a object of \Drupal\mongodb\Driver\Condition.
    return new Condition($conjunction);
  }

}
