<?php

namespace Drupal\mongodb\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\mongodb\Driver\Statement;
use Drupal\mongodb\Driver\StatementCountQuery;

/**
 * The MongoDB service for transalting SQL queries.
 */
class TranslateSql {

  /**
   * The current database connection.
   *
   * @var \Drupal\mongodb\Driver\Connection
   */
  protected $connection;

  /**
   * The query translation data.
   *
   * @var array
   */
  protected $translations = [
    [
      'pattern' => '/^SELECT cid, data, created, expire, serialized, tags, checksum FROM {(.*)} WHERE cid IN \( :cids\[\] \) ORDER BY cid$/',
      'filter' => ['cid' => ['$in' => ':cids']],
      'projection' => ['cid' => 1, 'data' => 1, 'created' => 1, 'expire' => 1, 'serialized' => 1, 'tags' => 1, 'checksum' => 1],
    ],
    [
      'pattern' => '/^SELECT name, value FROM {(.*)} WHERE name IN \( :keys\[\] \) AND collection = :collection$/',
      'filter' => ['name' => ['$in' => ':keys[]'], 'collection' => ':collection'],
      'projection' => ['name' => 1, 'value' => 1],
    ],
    [
      'pattern' => '/^SELECT tag, invalidations FROM {(.*)} WHERE tag IN \( :tags\[\] \)$/',
      'filter' => ['tag' => ['$in' => ':tags[]']],
      'projection' => ['tag' => 1, 'invalidations' => 1],
    ],
    [
      'pattern' => '/^SELECT data FROM {(.*)} WHERE collection = :collection AND name = :name$/',
      'filter' => ['collection' => ':collection', 'name' => ':name'],
      'projection' => ['data' => 1],
    ],
    [
      'pattern' => '/^SELECT name, data FROM {(.*)} WHERE collection = :collection AND name IN \( :names\[\] \)$/',
      'filter' => ['name' => ['$in' => ':names[]'], 'collection' => ':collection'],
      'projection' => ['name' => 1, 'data' => 1],
    ],
    [
      'pattern' => '/^SELECT name, value FROM {(.*)} WHERE collection = :collection$/',
      'filter' => ['collection' => ':collection'],
      'projection' => ['name' => 1, 'value' => 1],
    ],
    [
      'pattern' => '/^SELECT session FROM {(.*)} WHERE sid = :sid$/',
      'filter' => ['sid' => ':sid'],
      'projection' => ['session' => 1],
    ],
    [
      'pattern' => '/^SELECT name, route FROM {(.*)} WHERE name IN \( :names\[\] \)$/',
      'filter' => ['name' => ['$in' => ':names[]']],
      'projection' => ['name' => 1, 'route' => 1],
    ],

    // Query_range queries.
    [
      'pattern' => '/^SELECT 1 FROM {(.*)} WHERE collection = :collection AND name = :name$/',
      'filter' => ['name' => ':name', 'collection' => ':collection'],
      'projection' => [],
    ],

    // Test queries.
    [
      'pattern' => '/^SELECT name FROM {(.*)} WHERE age = :age$/',
      'filter' => ['age' => ':age'],
      'projection' => ['name' => 1],
    ],
    [
      'pattern' => '/^SELECT COUNT\(\*\) FROM {(.*)}$/',
      'filter' => [],
      'projection' => [],
      'count' => 1,
    ],
    [
      'pattern' => '/^SELECT \* FROM {(.*)} WHERE job = :job$/',
      'filter' => ['job' => ':job'],
      'projection' => [],
    ],
    [
      'pattern' => '/^SELECT classname, name, job FROM {(.*)} WHERE age = :age$/',
      'filter' => ['age' => ':age'],
      'projection' => ['classname' => 1, 'name' => 1, 'job' => 1, '_id' => 0],
    ],
    [
      'pattern' => '/^SELECT name FROM {(.*)}$/',
      'filter' => [],
      'projection' => ['name' => 1],
    ],
    [
      'pattern' => '/^SELECT age FROM {(.*)} WHERE name = :name$/',
      'filter' => ['name' => ':name'],
      'projection' => ['age' => 1],
    ],
    [
      'pattern' => '/^SELECT "offset" FROM {(.*)} WHERE id = :id$/',
      'filter' => ['id' => ':id'],
      'projection' => ['offset' => 1],
    ],
    [
      'pattern' => '/^SELECT \* FROM {(.*)}$/',
      'filter' => [],
      'projection' => ['id' => 1, 'name' => 1, 'age' => 1, 'job' => 1],
    ],
    [
      'pattern' => '/^SELECT \* FROM {(.*)} WHERE name = :name$/',
      'filter' => ['name' => ':name'],
      'projection' => ['id' => 1, 'name' => 1, 'age' => 1, 'job' => 1],
    ],
    [
      'pattern' => '/^SELECT age FROM {(.*)} WHERE job = :job$/',
      'filter' => ['job' => ':job'],
      'projection' => ['age' => 1],
    ],
    [
      'pattern' => '/^SELECT name FROM {(.*)} WHERE id = :id$/',
      'filter' => ['id' => ':id'],
      'projection' => ['name' => 1],
    ],
    [
      'pattern' => '/^SELECT COUNT\(\*\) FROM {(.*)} WHERE job = :job$/',
      'filter' => ['job' => ':job'],
      'projection' => [],
      'count' => 1,
    ],
    [
      'pattern' => '/^SELECT name FROM {(.*)} WHERE age IN \( :ages\[\] \) ORDER BY age$/',
      'filter' => ['age' => ['$in' => ':ages[]']],
      'projection' => ['name' => 1],
      'sort' => ['age' => 1],
    ],
    [
      'pattern' => '/^SELECT job FROM {(.*)} WHERE id = :id$/',
      'filter' => ['id' => ':id'],
      'projection' => ['job' => 1],
      'integer_arguments' => [':id'],
    ],
    [
      'pattern' => '/^SELECT 1 FROM {(.*)} WHERE name = :name$/',
      'filter' => ['name' => ':name'],
      'projection' => [],
    ],
    [
      'pattern' => '/^SELECT job FROM {(.*)} WHERE job = :job$/',
      'filter' => ['job' => ':job'],
      'projection' => ['job' => 1],
    ],
    [
      'pattern' => '/^SELECT data FROM {(.*)} WHERE name = :name$/',
      'filter' => ['name' => ':name'],
      'projection' => ['data' => 1],
    ],
    [
      'pattern' => '/^SELECT \* FROM {(.*)} WHERE source = :source AND alias= :alias AND langcode = :langcode$/',
      'filter' => ['source' => ':source', 'alias' => ':alias', 'langcode' => ':langcode'],
      'projection' => ['pid' => 1, 'source' => 1, 'alias' => 1, 'langcode' => 1],
    ],
    [
      'pattern' => '/^SELECT pid FROM {(.*)} WHERE source = :source AND alias= :alias AND langcode = :langcode$/',
      'filter' => ['source' => ':source', 'alias' => ':alias', 'langcode' => ':langcode'],
      'projection' => ['pid' => 1],
    ],
    [
      'pattern' => '/^SELECT \* FROM {(.*)} WHERE pid = :pid$/',
      'filter' => ['pid' => ':pid'],
      'projection' => ['pid' => 1, 'source' => 1, 'alias' => 1, 'langcode' => 1],
    ],
    [
      'pattern' => '/^SELECT name, path, pattern_outline, fit, route FROM {(.*)} WHERE name = :name$/',
      'filter' => ['name' => ':name'],
      'projection' => ['name' => 1, 'path' => 1, 'pattern_outline' => 1, 'fit' => 1, 'route' => 1],
    ],
    [
      'pattern' => '/^SELECT expire, value FROM {(.*)} WHERE name = :name$/',
      'filter' => ['name' => ':name'],
      'projection' => ['expire' => 1, 'value' => 1],
    ],
  ];

  /**
   * Constructs the TranslateSql object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to use.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Helper function to create the fields array from the projection.
   */
  protected function projectionToFields(array $projection) {
    $fields = [];
    foreach ($projection as $key => $value) {
      if ($value == 1) {
        $fields[] = $key;
      }
    }
    return $fields;
  }

  /**
   * Translate a query from Connection::query and execute the query.
   */
  public function query($query, array $args = [], array $query_options = []) {
    // Get the database query logger.
    $logger = $this->connection->getLogger();

    $options = [];
    $cursor = NULL;
    if (is_string($query)) {
      // Add the range data to the MongoDB options (skip and limit).
      if (!empty($query_options['start']) && !empty($query_options['length'])) {
        $options['skip'] = (int) $query_options['start'];
        $options['limit'] = (int) $query_options['length'];
        unset($query_options['start']);
        unset($query_options['length']);
      }

      foreach ($this->translations as $translation) {
        $matches = NULL;
        if (preg_match($translation['pattern'], $query, $matches)) {
          $filter = $translation['filter'];

          // MongoDB is strict about integer values being an integer.
          foreach ($args as $arg_key => $arg_value) {
            if (!empty($translation['integer_arguments']) && in_array($arg_key, $translation['integer_arguments'])) {
              $args[$arg_key] = (int) $arg_value;
            }
          }

          // Replace the placeholders with the real values.
          array_walk_recursive($filter, function(&$item_value, $item_key, $args) {
            foreach ($args as $arg_key => $arg_value) {
              if ($item_value == $arg_key) {
                $is_bracket_placeholder = substr($arg_key, -2) === '[]';
                $is_array_data = is_array($arg_value);
                if ($is_bracket_placeholder && !$is_array_data) {
                  throw new \InvalidArgumentException('Placeholders with a trailing [] can only be expanded with an array of values.');
                }
                if (!$is_bracket_placeholder && $is_array_data) {
                  throw new \InvalidArgumentException('Placeholders must have a trailing [] if they are to be expanded with an array of values.');
                }

                $item_value = $arg_value;
              }
            }
          }, $args);

          // Add the fields to the MongoDB projection.
          if (!empty($translation['projection'])) {
            $options['projection'] = $translation['projection'];
          }

          $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);

          if (!empty($translation['count'])) {
            // Get the query start time for the database query logger.
            $query_start = !empty($logger) ? microtime(TRUE) : 0;

            // Execute the count query.
            $count = $this->connection->getConnection()->{$prefixed_table}->count(
              $filter,
              $options
            );

            // Get the query end time for the database query logger.
            $query_end = !empty($logger) ? microtime(TRUE) : 0;
            $query_options['query_time'] = $query_end - $query_start;

            return new StatementCountQuery($this->connection, $count, $query_options);
          }

          // Add the sorting to the MongoDB query.
          if (!empty($translation['sort'])) {
            $options['sort'] = $translation['sort'];
          }

          // Get the query start time for the database query logger.
          $query_start = !empty($logger) ? microtime(TRUE) : 0;

          // Execute the query.
          $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
            $filter,
            $options
          );

          // Get the query end time for the database query logger.
          $query_end = !empty($logger) ? microtime(TRUE) : 0;
          $query_options['query_time'] = $query_end - $query_start;

          // If we have a MongoDB query result cursor then return the Statement
          // object.
          if ($cursor) {
            $statement = new Statement($this->connection, $cursor, $this->projectionToFields($translation['projection']));
            $statement->execute(NULL, $query_options);
            return $statement;
          }
          break;
        }
      }

      // The SQL queries that need special handling.
      if (preg_match('/^SELECT MAX\(id\) FROM {(.*)}$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
          [],
          [
            'sort' => ['id' => -1],
            'limit' => 1,
            'projection' => ['id' => 1, '_id' => 0]
          ]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['id']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^SELECT name FROM {(.*)} WHERE age > :age$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);
        $age = $args[':age'];

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
          ['age' => ['$gt' => $age]],
          ['projection' => ['name' => 1, '_id' => 0]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['name']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^SELECT \* FROM {test_one_blob} WHERE id = :id$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable('test_one_blob');
        $id = (int) $args[':id'];

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
          ['id' => ['$eq' => $id]],
          ['projection' => ['id' => 1, 'blob1' => 1, '_id' => 0]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['id', 'blob1']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^SELECT \* FROM {test_two_blobs} WHERE id = :id$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable('test_two_blobs');
        $id = (int) $args[':id'];

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
          ['id' => ['$eq' => $id]],
          ['projection' => ['id' => 1, 'blob1' => 1, 'blob2' => 1, '_id' => 0]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['id', 'blob1', 'blob2']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^SELECT \* FROM {test_special_columns} WHERE id = :id$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable('test_special_columns');
        $id = (int) $args[':id'];

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
          ['id' => ['$eq' => $id]],
          ['projection' => ['id' => 1, 'offset' => 1, 'function' => 1, '_id' => 0]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['id', 'offset', 'function']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^SELECT name FROM {(.*)} ORDER BY name$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
          [],
          [
            'projection' => ['name' => 1, '_id' => 0],
            'sort' => ['name' => 1],
            'skip' => isset($options['skip']) ? $options['skip'] : 0,
            'limit' => isset($options['limit']) ? $options['limit'] : 0,
          ]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['name']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^SELECT MAX\(test_serial\) FROM {(.*)}$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->aggregate(
          [
            ['$group' => [
              '_id' => '$test_serial',
              'max' => ['$max' => '$test_serial']
            ]],
            ['$project' => ['max' => 1, '_id' => 0]],
          ],
          ['useCursor' => TRUE]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['max']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^SELECT COUNT\(\*\) >= 3 FROM {(.*)}$/', $query, $matches) || preg_match('/^SELECT COUNT\(\*\) >= :count FROM {(.*)}$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);
        $count = isset($args[':count']) ? intval($args[':count']) : 3;

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        // Execute the count query.
        $count = count($this->connection->getConnection()->{$prefixed_table}->count(
          [],
          [
            ['$group' => [
              '_id' => '$id',
              'count' => ['$sum' => 1]
            ]],
            ['$project' => ['count' => 1, '_id' => 0]],
            ['$match' => ['$gte' => ['$count', $count]]]
          ],
          ['useCursor' => FALSE]
        ));

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        return new StatementCountQuery($this->connection, $count, $query_options);
      }
      elseif (preg_match('/^SELECT name, value FROM {(.*)} WHERE collection = :collection AND expire > :now$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);
        $collection = $args[':collection'];
        $now = $args[':now'];

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $cursor = $this->connection->getConnection()->{$prefixed_table}->find(
          ['collection' => ['$eq' => $collection], 'expire' => ['$gt' => $now]],
          ['projection' => ['name' => 1, 'value' => 1, '_id' => 0]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        $statement = new Statement($this->connection, $cursor, ['name', 'value']);
        $statement->execute(NULL, $query_options);
        return $statement;
      }
      elseif (preg_match('/^UPDATE {(.*)} SET uid = 1 WHERE id = 1$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $result = $this->connection->getConnection()->{$prefixed_table}->updateMany(
          ['id' => 1],
          ['$set' => ['uid' => 1]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        if ($result && $result->isAcknowledged()) {
          // Return the number of rows matched by query.
          return $result->getMatchedCount();
        }
      }
      elseif (preg_match('/^UPDATE {(.*)} SET uid = 2 WHERE id <> 1$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $result = $this->connection->getConnection()->{$prefixed_table}->updateMany(
          ['id' => ['$ne' => 1]],
          ['$set' => ['uid' => 2]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        if ($result && $result->isAcknowledged()) {
          // Return the number of rows matched by query.
          return $result->getMatchedCount();
        }
      }
      elseif (preg_match('/^UPDATE {(.*)} SET uid = :uid WHERE id = (\d)$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);
        $uid = (int) $args[':uid'];
        $id = (int) $matches[2];

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $result = $this->connection->getConnection()->{$prefixed_table}->updateMany(
          ['id' => $id],
          ['$set' => ['uid' => $uid]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        if ($result && $result->isAcknowledged()) {
          // Return the number of rows matched by query.
          return $result->getMatchedCount();
        }
      }
      elseif (preg_match('/^UPDATE {(.*)} SET uid = :uid WHERE id IN \((\d),(\d)\)$/', $query, $matches)) {
        $prefixed_table = $this->connection->getMongodbPrefixedTable($matches[1]);
        $uid = (int) $args[':uid'];
        $first_id = (int) $matches[2];
        $second_id = (int) $matches[3];

        // Get the query start time for the database query logger.
        $query_start = !empty($logger) ? microtime(TRUE) : 0;

        $result = $this->connection->getConnection()->{$prefixed_table}->updateMany(
          ['id' => ['$in' => [$first_id, $second_id]]],
          ['$set' => ['uid' => $uid]]
        );

        // Get the query end time for the database query logger.
        $query_end = !empty($logger) ? microtime(TRUE) : 0;
        $query_options['query_time'] = $query_end - $query_start;

        if ($result && $result->isAcknowledged()) {
          // Return the number of rows matched by query.
          return $result->getMatchedCount();
        }
      }

      // Unable to translate and execute the SQL statement.
      throw new DatabaseExceptionWrapper('Unknown query string: ' . $query);
    }
    else {
      // An SQL statement must be a string.
      throw new DatabaseExceptionWrapper('The query is not a string!');
    }
  }

  /**
   * Translate a query with the range data from Connection::query_range and execute the query.
   */
  public function queryRange($query, $from, $count, array $args = [], array $options = []) {
    $options['start'] = (int) $from;
    $options['length'] = (int) $count;

    return $this->query($query, $args, $options);
  }

}
