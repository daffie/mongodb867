<?php

namespace Drupal\mongodb\modules\file;

use Drupal\file\FileInterface;
use Drupal\file\FileUsage\DatabaseFileUsageBackend as CoreDatabaseFileUsageBackend;

/**
 * The MongoDB implementation of \Drupal\file\FileUsage\DatabaseFileUsageBackend.
 */
class DatabaseFileUsageBackend extends CoreDatabaseFileUsageBackend {

  /**
   * {@inheritdoc}
   */
  public function add(FileInterface $file, $module, $type, $id, $count = 1) {
    $prefixed_table = $this->connection->getMongodbPrefixedTable($this->tableName);
    $result = $this->connection->getConnection()->{$prefixed_table}->updateMany(
      [
        'fid' => (int) $file->id(),
        'module' => $module,
        'type' => $type,
        'id' => (string) $id,
      ],
      [
        '$inc' => [ 'count' => $count ]
      ],
      [
        'upsert' => TRUE
      ]
    );

    // Make sure that a used file is permanent.
    if (!$file->isPermanent()) {
      $file->setPermanent();
      $file->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete(FileInterface $file, $module, $type = NULL, $id = NULL, $count = 1) {
    // Delete rows that have a exact or less value to prevent empty rows.
    $query = $this->connection->delete($this->tableName)
      ->condition('module', (string) $module)
      ->condition('fid', (int) $file->id());
    if ($type && $id) {
      $query
        ->condition('type', (string) $type)
        ->condition('id', (string) $id);
    }
    if ($count) {
      $query->condition('count', $count, '<=');
    }
    $result = $query->execute();

    // If the row has more than the specified count decrement it by that number.
    if (!$result && $count > 0) {
      $conditions = [
        'module' => (string) $module,
        'fid' => (int) $file->id()
      ];
      if ($type && $id) {
        $conditions['type'] = (string) $type;
        $conditions['id'] = (string) $id;
      }
      $prefixed_table = $this->connection->getMongodbPrefixedTable($this->tableName);
      $result = $this->connection->getConnection()->{$prefixed_table}->updateMany(
        $conditions,
        [
          '$inc' => [ 'count' => ($count * -1) ]
        ]
      );
    }

    // Do not actually mark files as temporary when the behavior is disabled.
    if (!$this->configFactory->get('file.settings')->get('make_unused_managed_files_temporary')) {
      return;
    }

    // If there are no more remaining usages of this file, mark it as temporary,
    // which result in a delete through system_cron().
    $usage = \Drupal::service('file.usage')->listUsage($file);
    if (empty($usage)) {
      $file->setTemporary();
      $file->save();
    }
  }

}
