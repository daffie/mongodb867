<?php

namespace Drupal\mongodb\modules\book;

use Drupal\book\BookOutlineStorage as CoreBookOutlineStorage;

/**
 * The MongoDB implementation of \Drupal\book\BookOutlineStorage.
 */
class BookOutlineStorage extends CoreBookOutlineStorage {

  /**
   * {@inheritdoc}
   */
  public function getBooks() {
    $result = $this->connection->select('book', 'b')
      ->fields('b', ['bid'])
      ->execute()
      ->fetchAll();

    $bids = [];
    foreach ($result as $row) {
      if (isset($row->bid) && !in_array($row->bid, $bids)) {
        $bids[] = $row->bid;
      }
    }

    return $bids;
  }

  /**
   * {@inheritdoc}
   */
  public function hasBooks() {
    return (bool) $this->connection->select('book', 'b')
      ->fields('b', ['bid'])
      ->range(0, 1)
      ->execute()
      ->fetchfield();
  }

  /**
   * {@inheritdoc}
   */
  public function loadBookChildren($pid) {
    return $this->connection->select('book', 'b')
      ->fields('b')
      ->condition('pid', (int) $pid)
      ->execute()
      ->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);
  }

}
