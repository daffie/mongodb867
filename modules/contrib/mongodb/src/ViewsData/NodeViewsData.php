<?php

namespace Drupal\mongodb\ViewsData;

use Drupal\Component\Utility\NestedArray;

/**
 * The MongoDB implementation of \Drupal\node\NodeViewsData.
 */
class NodeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();


    $extra = node_views_data_extra();

    $extra_field_data['node'] = $extra['node_field_data'];
    unset($extra['node_field_data']);

    $extra_field_revision['node']['link_to_revision'] = $extra['node_field_revision']['link_to_revision'];
    $extra_field_revision['node']['revert_revision'] = $extra['node_field_revision']['revert_revision'];
    $extra_field_revision['node']['delete_revision'] = $extra['node_field_revision']['delete_revision'];
    unset($extra['node_field_revision']);

    $extra['node_access']['table']['join']['node'] = $extra['node_access']['table']['join']['node_field_data'];
    unset($extra['node_access']['table']['join']['node_field_data']);

    unset($extra['node_search_dataset']['table']['join']['node']['left_field']);
    unset($extra['node_search_dataset']['table']['join']['node']['left_table']);
    unset($extra['node_search_dataset']['table']['join']['node']['extra']);
    unset($extra['node_search_dataset']['table']['join']['node']['type']);

    $extra['node']['path'] = [
      'field' => [
        'title' => $this->t('Path'),
        'help' => $this->t('The aliased path to this content.'),
        'id' => 'node_path',
      ],
    ];

    // The next line is added for MongoDB. No idea why!
    $extra['node']['uid']['relationship']['base'] = 'users';

    $data = NestedArray::mergeDeep($data, $extra, $extra_field_data, $extra_field_revision);

    return $data;
  }

}
