<?php

namespace Drupal\mongodb\ViewsData;

use Drupal\Component\Utility\NestedArray;

/**
 * The MongoDB implementation of \Drupal\file\FileViewsData.
 */
class FileViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $extra = file_views_data_extra();

    // For MongoDB the join result needs to be unwound.
    $extra['file_usage']['table']['join']['file_managed']['one_to_many'] = TRUE;

    $data = NestedArray::mergeDeep($data, $extra);

    return $data;
  }

}
