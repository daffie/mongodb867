<?php

namespace Drupal\mongodb\ViewsData;

use Drupal\Component\Utility\NestedArray;

/**
 * The MongoDB implementation of \Drupal\block_content\BlockContentViewsData.
 */
class BlockContentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $extra = block_content_views_data_extra();

    $extra_field_data['block_content'] = $extra['block_content_field_data'];
    unset($extra['block_content_field_data']);

    $extra_field_revision['block_content'] = $extra['block_content_field_revision'];
    $extra_field_revision['block_content']['id']['relationship']['base'] = 'block_content';
    $extra_field_revision['block_content']['revision_id']['relationship']['base'] = 'block_content';
    unset($extra['block_content_field_revision']);

    $data = NestedArray::mergeDeep($data, $extra, $extra_field_data, $extra_field_revision);

    return $data;
  }

}
