<?php

namespace Drupal\mongodb\ViewsData;

use Drupal\Component\Utility\NestedArray;

/**
 * The MongoDB implementation of \Drupal\comment\CommentViewsData.
 */
class CommentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $extra = comment_views_data_extra();

    $extra_field_data['comment'] = $extra['comment_field_data'];
    unset($extra['comment_field_data']);

    $data = NestedArray::mergeDeep($data, $extra, $extra_field_data);

    return $data;
  }

}
