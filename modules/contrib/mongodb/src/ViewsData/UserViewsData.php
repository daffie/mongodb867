<?php

namespace Drupal\mongodb\ViewsData;

use Drupal\Component\Utility\NestedArray;

/**
 * The MongoDB implementation of \Drupal\user\UserViewsData.
 */
class UserViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $extra = user_views_data_extra();

    $extra_field_data['users'] = $extra['users_field_data'];
    unset($extra['users_field_data']);

    $extra_field_data['users']['uid']['argument']['name table'] = 'users';
    $extra_field_data['users']['uid']['relationship']['base'] = 'node';

    $extra_field_data['users']['uid_representative']['relationship']['outer field'] = 'users.uid';
    $extra_field_data['users']['uid_representative']['relationship']['argument table'] = 'users';
    $extra_field_data['users']['uid_representative']['relationship']['base'] = 'node';
    $extra_field_data['users']['uid_representative']['relationship']['relationship'] = 'node:uid';

    $extra_roles['users'] = $extra['user__roles'];
    unset($extra['user__roles']);

    $extra_roles['users']['roles_target_id']['title'] = $this->t('Roles');
    $extra_roles['users']['roles_target_id']['help'] = $this->t('Roles that a user belongs to.');

    $data = NestedArray::mergeDeep($data, $extra, $extra_field_data, $extra_roles);

    return $data;
  }

}
