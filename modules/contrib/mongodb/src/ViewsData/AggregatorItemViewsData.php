<?php

namespace Drupal\mongodb\ViewsData;

use Drupal\Component\Utility\NestedArray;

/**
 * The MongoDB implementation of \Drupal\aggregator\AggregatorItemViewsData.
 */
class AggregatorItemViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data = NestedArray::mergeDeep($data, aggregator_item_views_data_extra());

    return $data;
  }

}
