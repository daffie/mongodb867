<?php

namespace Drupal\mongodb\ViewsData;

use Drupal\Component\Utility\NestedArray;

/**
 * The MongoDB implementation of \Drupal\taxonomy\TermViewsData.
 */
class TermViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $extra = taxonomy_term_views_data_extra();

    $extra_field_data['taxonomy_term_data'] = $extra['taxonomy_term_field_data'];
    unset($extra['taxonomy_term_field_data']);

    $extra_field_data['taxonomy_term_data']['tid']['filter']['hierarchy table'] = 'taxonomy_term_data';

    $extra_field_data['taxonomy_term_data']['tid_representative']['relationship']['outer field'] = 'taxonomy_term_data.tid';
    $extra_field_data['taxonomy_term_data']['tid_representative']['relationship']['argument table'] = 'taxonomy_term_data';
    $extra_field_data['taxonomy_term_data']['tid_representative']['relationship']['base'] = 'node';
    $extra_field_data['taxonomy_term_data']['tid_representative']['relationship']['relationship'] = 'node:term_node_tid';

    $extra['taxonomy_index']['table']['join']['taxonomy_term_data'] = $extra['taxonomy_index']['table']['join']['taxonomy_term_field_data'];
    unset($extra['taxonomy_index']['table']['join']['taxonomy_term_field_data']);
    $extra['taxonomy_index']['table']['join']['node'] = $extra['taxonomy_index']['table']['join']['node_field_data'];
    unset($extra['taxonomy_index']['table']['join']['node_field_data']);
    unset($extra['taxonomy_index']['table']['join']['taxonomy_term__parent']);

    $extra['taxonomy_index']['tid']['argument']['name table'] = 'taxonomy_term_data';
    $extra['taxonomy_index']['tid']['argument']['skip base'] = 'taxonomy_term_data';
    $extra['taxonomy_index']['tid']['filter']['hierarchy table'] = 'taxonomy_term_data';
    $extra['taxonomy_index']['tid']['filter']['skip base'] = 'taxonomy_term_data';

    $data = NestedArray::mergeDeep($data, $extra, $extra_field_data);

    return $data;
  }

}
