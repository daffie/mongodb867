<?php

namespace Drupal\block_content\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\content_translation\Plugin\migrate\source\I18nQueryTrait;

/**
 * Gets Drupal 7 custom block translation from database.
 *
 * @MigrateSource(
 *   id = "d7_block_custom_translation",
 *   source_module = "block"
 * )
 */
class BlockCustomTranslation extends DrupalSqlBase {

  use I18nQueryTrait;

  /**
   * Drupal 7 table names.
   */
  const CUSTOM_BLOCK_TABLE = 'block_custom';
  const I18N_STRING_TABLE = 'i18n_string';

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Build a query based on blockCustomTable table where each row has the
    // translation for only one property, either title or description. The
    // method prepareRow() is then used to obtain the translation for the
    // other property.
    $query = $this->select(static::CUSTOM_BLOCK_TABLE, 'b')
      ->fields('b', ['bid', 'format', 'body'])
      ->fields('i18n', ['property'])
      ->fields('lt', ['lid', 'translation', 'language'])
      ->orderBy('b.bid');

    // Use 'title' for the info field to match the property name in
    // i18nStringTable.
    $query->addField('b', 'info', 'title');

    // Add in the property, which is either title or body. Cast the bid to text
    // so PostgreSQL can make the join.
    $query->addMongodbJoin('LEFT', static::I18N_STRING_TABLE, 'objectid', static::CUSTOM_BLOCK_TABLE, 'bid', '=', 'i18n', [['field' => 'type', 'value' => 'block']]);

    // Add in the translation for the property.
    $query->addMongodbJoin('LEFT', 'locales_target', 'lid', static::I18N_STRING_TABLE, 'lid', '=', 'lt');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $table_info = $this->getDatabase()->tableInformation();
      $custom_block_table_bid_data = $table_info->getTableField(static::CUSTOM_BLOCK_TABLE, 'bid');
      if (isset($custom_block_table_bid_data['type']) && in_array($custom_block_table_bid_data['type'], ['int', 'serial'])) {
        $custom_block_table_bid_is_integer = TRUE;
      }
      else {
        $custom_block_table_bid_is_integer = FALSE;
      }

      $i18n_string_table_objectid_data = $table_info->getTableField(static::I18N_STRING_TABLE, 'objectid');
      if (isset($i18n_string_table_objectid_data['type']) && in_array($i18n_string_table_objectid_data['type'], ['int', 'serial'])) {
        $i18n_string_table_objectid_is_integer = TRUE;
      }
      else {
        $i18n_string_table_objectid_is_integer = FALSE;
      }

      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        if (($custom_block_table_bid_is_integer xor $i18n_string_table_objectid_is_integer) && (!empty($row['bid']))) {
          if ($i18n_string_table_objectid_is_integer) {
            $bid = (int) $row['bid'];
          }
          else {
            $bid = (string) $row['bid'];
          }
          $query = $this->select(static::I18N_STRING_TABLE, 'i18n')
            ->fields('i18n', ['property'])
            ->fields('lt', ['lid', 'translation', 'language'])
            ->condition('i18n.type', 'block')
            ->condition('i18n.objectid', $bid);
          $query->addMongodbJoin('LEFT', 'locales_target', 'lid', static::I18N_STRING_TABLE, 'lid', '=', 'lt');
          $query->addFilterUnwindPath('lt');
          $results = $query->execute()->fetchAll();
          foreach ($results as $result) {
            $row = array_merge($row, $result);

            // The implementation of $query->isNotNull('i18n.lid')
            if (!empty($row['lid'])) {
              $rows[] = $row;
            }
          }
        }
        else {
          foreach (['lid', 'translation', 'language'] as $field_name) {
            if (isset($row[$field_name]) && is_array($row[$field_name])) {
              $row[$field_name] = reset($row[$field_name]);
            }
            if (isset($row['lt_' . $field_name])) {
              unset($row['lt_' . $field_name]);
            }
          }
          if (isset($row['i18n_property'])) {
            if (is_array($row['i18n_property'])) {
              $row['property'] = reset($row['i18n_property']);
            }
            else {
              $row['property'] = $row['i18n_property'];
            }
            unset($row['i18n_property']);
          }

          // The implementation of $query->isNotNull('i18n.lid')
          if (!empty($row['lid'])) {
            $rows[] = $row;
          }
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    parent::prepareRow($row);
    // Set the i18n string table for use in I18nQueryTrait.
    $this->i18nStringTable = static::I18N_STRING_TABLE;
    // Save the translation for this property.
    $property_in_row = $row->getSourceProperty('property');
    // Get the translation for the property not already in the row and save it
    // in the row.
    $property_not_in_row = ($property_in_row === 'title') ? 'body' : 'title';
    $result = $this->getPropertyNotInRowTranslation($row, $property_not_in_row, 'bid', $this->idMap);
    foreach (['body_translated', 'title_translated'] as $field_name) {
      if ($field_value = $row->getSourceProperty($field_name)) {
        if (is_array($field_value)) {
          $row->setSourceProperty($field_name, reset($field_value));
        }
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'bid' => $this->t('The block numeric identifier.'),
      'format' => $this->t('Input format of the custom block/box content.'),
      'lid' => $this->t('i18n_string table id'),
      'language' => $this->t('Language for this field.'),
      'property' => $this->t('Block property'),
      'translation' => $this->t('The translation of the value of "property".'),
      'title' => $this->t('Block title.'),
      'title_translated' => $this->t('Block title translation.'),
      'body' => $this->t('Block body.'),
      'body_translated' => $this->t('Block body translation.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['bid']['type'] = 'integer';
    $ids['bid']['alias'] = 'b';
    $ids['language']['type'] = 'string';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this
      ->initializeIterator()
      ->count();
  }

}
