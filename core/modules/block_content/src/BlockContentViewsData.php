<?php

namespace Drupal\block_content;

use Drupal\Component\Utility\NestedArray;
use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the block_content entity type.
 */
class BlockContentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {

    $data = parent::getViewsData();

    $data = NestedArray::mergeDeep($data, block_content_views_data_extra());

    return $data;
  }

}
