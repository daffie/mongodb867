<?php

namespace Drupal\config_translation\Plugin\migrate\source\d6;

use Drupal\Core\Database\Database;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use MongoDB\BSON\Binary;

/**
 * Gets i18n strings profile field source from database.
 *
 * @MigrateSource(
 *   id = "d6_profile_field_translation",
 *   source_module = "i18nprofile"
 * )
 */
class ProfileFieldTranslation extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('profile_fields', 'pf')
      ->fields('pf', ['fid', 'name']);

    $query->addMongodbJoin('LEFT', 'i18n_strings', 'objectid', 'profile_fields', 'name', '=', 'i18n');
    $query->fields('i18n', ['property']);

    $query->addMongodbJoin('LEFT', 'locales_target', 'lid', 'i18n_strings', 'lid', '=', 'lt');
    $query->fields('lt', ['lid', 'translation', 'language']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'fid' => $this->t('Profile field ID.'),
      'lid' => $this->t('Locales target language ID.'),
      'language' => $this->t('Language for this field.'),
      'translation' => $this->t('Translation of either the title or explanation.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['fid']['type'] = 'integer';
    $ids['language']['type'] = 'string';
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $ids['language']['alias'] = 'lt';
    }
    $ids['lid']['type'] = 'integer';
    $ids['lid']['alias'] = 'lt';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        if (isset($row['lt_lid']) && is_array($row['lt_lid'])) {
          $row['lid'] = reset($row['lt_lid']);
          unset($row['lt_lid']);
        }
        if (is_array($row['lt_language']) && is_array($row['lt_language'])) {
          $row['language'] = reset($row['lt_language']);
          unset($row['lt_language']);
        }
        if (isset($row['lt_translation']) && is_array($row['lt_translation'])) {
          $row['translation'] = reset($row['lt_translation']);
          if ($row['translation'] instanceof Binary) {
            $row['translation'] = $row['translation']->getData();
          }
          unset($row['lt_translation']);
        }
        if (isset($row['i18n_property'])) {
          $row['property'] = $row['i18n_property'];
          unset($row['i18n_property']);
        }

        // The implementation of $query->isNotNull('language')
        if (!empty($row['language'])) {
          $rows[] = $row;
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      return $this->initializeIterator()->count();
    }
    else {
      return parent::count();
    }
  }

}
