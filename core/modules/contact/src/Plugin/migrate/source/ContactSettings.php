<?php

namespace Drupal\contact\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\Variable;

/**
 * @MigrateSource(
 *   id = "contact_settings",
 *   source_module = "contact"
 * )
 */
class ContactSettings extends Variable {

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $selected = '1';
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $contact_selected_data = $this->getDatabase()->tableInformation()->getTableField('contact', 'selected');
      if (isset($contact_selected_data['type']) && in_array($contact_selected_data['type'], ['int', 'serial'])) {
        $selected = 1;
      }
    }

    $default_category = $this->select('contact', 'c')
      ->fields('c', ['cid'])
      ->condition('selected', $selected)
      ->execute()
      ->fetchField();
    return new \ArrayIterator([$this->values() + ['default_category' => $default_category]]);
  }

}
