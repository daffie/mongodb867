<?php

namespace Drupal\book\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 6 and 7 book source.
 *
 * @MigrateSource(
 *   id = "book",
 *   source_module = "book",
 * )
 */
class Book extends DrupalSqlBase {

  /**
   * The list of fields from the menu_links table.
   *
   * @var array
   */
  protected $menu_link_fields = ['mlid', 'plid', 'weight', 'has_children', 'depth', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9'];

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('book', 'b')->fields('b', ['nid', 'bid']);
    $query->addMongodbJoin('LEFT', 'menu_links', 'mlid', 'book', 'mlid', '=', 'ml');
    $ml_fields = ['mlid', 'plid', 'weight', 'has_children', 'depth'];
    foreach (range(1, 9) as $i) {
      $field = "p$i";
      $ml_fields[] = $field;
      $query->orderBy('ml.' . $field);
    }
    $query->fields('ml', $this->menu_link_fields);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['mlid']['type'] = 'integer';
    $ids['mlid']['alias'] = 'ml';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'nid' => $this->t('Node ID'),
      'bid' => $this->t('Book ID'),
      'mlid' => $this->t('Menu link ID'),
      'plid' => $this->t('Parent link ID'),
      'weight' => $this->t('Weight'),
      'p1' => $this->t('The first mlid in the materialized path. If N = depth, then pN must equal the mlid. If depth > 1 then p(N-1) must equal the parent link mlid. All pX where X > depth must equal zero. The columns p1 .. p9 are also called the parents.'),
      'p2' => $this->t('The second mlid in the materialized path. See p1.'),
      'p3' => $this->t('The third mlid in the materialized path. See p1.'),
      'p4' => $this->t('The fourth mlid in the materialized path. See p1.'),
      'p5' => $this->t('The fifth mlid in the materialized path. See p1.'),
      'p6' => $this->t('The sixth mlid in the materialized path. See p1.'),
      'p7' => $this->t('The seventh mlid in the materialized path. See p1.'),
      'p8' => $this->t('The eighth mlid in the materialized path. See p1.'),
      'p9' => $this->t('The ninth mlid in the materialized path. See p1.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        foreach ($this->menu_link_fields as $field) {
          $ml_field = 'ml_' . $field;
          if (isset($row[$ml_field])) {
            if (is_array($row[$ml_field])) {
              $row[$field] = reset($row[$ml_field]);
            }
            else {
              $row[$field] = $row[$ml_field];
            }
            unset($row[$ml_field]);
          }
        }
        $rows[] = $row;
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this
      ->initializeIterator()
      ->count();
  }

}
