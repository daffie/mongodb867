<?php

namespace Drupal\views\Plugin\views\filter;

/**
 * Simple filter to handle greater than/less than filters
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("groupby_numeric")
 */
class GroupByNumeric extends NumericFilter {

  public function query() {
    $this->ensureMyTable();
    $field = $this->getField();

    if ($this->view->getDatabaseDriver() == 'mongodb') {
      if ($this->table == $this->view->storage->get('base_table')) {
        $field = $this->realField;
      }
      else {
        $field = "$this->tableAlias.$this->realField";
      }
    }

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }

  protected function opBetween($field) {
    $placeholder_min = $this->placeholder();
    $placeholder_max = $this->placeholder();
    if ($this->operator == 'between') {
      if ($this->view->getDatabaseDriver() == 'mongodb') {
        $this->query->addGroupByOperation($placeholder_min, $field, 'MIN');
        $this->query->addGroupByOperation($placeholder_max, $field, 'MAX');
        $condition = $this->view->getDatabaseCondition('AND');
        $condition->condition($placeholder_min, $this->value['value'], '>=');
        $condition->condition($placeholder_max, $this->value['value'], '<=');
        $this->query->addHavingCondition($this->options['group'], $condition);
      }
      else {
        $this->query->addHavingExpression($this->options['group'], "$field >= $placeholder_min", [$placeholder_min => $this->value['min']]);
        $this->query->addHavingExpression($this->options['group'], "$field <= $placeholder_max", [$placeholder_max => $this->value['max']]);
      }
    }
    else {
      if ($this->view->getDatabaseDriver() == 'mongodb') {
        $this->query->addGroupByOperation($placeholder_min, $field, 'MIN');
        $this->query->addGroupByOperation($placeholder_max, $field, 'MAX');
        $condition = $this->view->getDatabaseCondition('OR');
        $condition->condition($placeholder_min, $this->value['value'], '<');
        $condition->condition($placeholder_max, $this->value['value'], '>');
        $this->query->addHavingCondition($this->options['group'], $condition);
      }
      else {
        $this->query->addHavingExpression($this->options['group'], "$field < $placeholder_min OR $field > $placeholder_max", [$placeholder_min => $this->value['min'], $placeholder_max => $this->value['max']]);
      }
    }
  }

  protected function opSimple($field) {
    $placeholder = $this->placeholder();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $this->query->addGroupByOperation($placeholder, $field, 'COUNT');
      $this->query->addHavingCondition($this->options['group'], $placeholder, $this->value['value'], $this->operator);
    }
    else {
      $this->query->addHavingExpression($this->options['group'], "$field $this->operator $placeholder", [$placeholder => $this->value['value']]);
    }
  }

  protected function opEmpty($field) {
    if ($this->operator == 'empty') {
      $operator = "IS NULL";
    }
    else {
      $operator = "IS NOT NULL";
    }

    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $this->query->addHavingCondition($this->options['group'], $field, NULL, $operator);
    }
    else {
      $this->query->addHavingExpression($this->options['group'], "$field $operator");
    }
  }

  public function adminLabel($short = FALSE) {
    return $this->getField(parent::adminLabel($short));
  }

  public function canGroup() {
    return FALSE;
  }

}
