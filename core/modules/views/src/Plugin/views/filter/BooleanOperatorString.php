<?php

namespace Drupal\views\Plugin\views\filter;

/**
 * Simple filter to handle matching of boolean values.
 *
 * This handler checks to see if a string field is empty (equal to '') or not.
 * It is otherwise identical to the parent operator.
 *
 * Definition items:
 * - label: (REQUIRED) The label for the checkbox.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("boolean_string")
 */
class BooleanOperatorString extends BooleanOperator {

  public function query() {
    $this->ensureMyTable();
    $where = "$this->tableAlias.$this->realField ";

    if (empty($this->value)) {
      $where .= "= ''";
      if ($this->accept_null) {
        if ($this->view->getDatabaseDriver() == 'mongodb') {
          $condition = $this->view->getDatabaseCondition('OR');
          $condition->condition("$this->tableAlias.$this->realField", '', self::EQUAL);
          $condition->isNull("$this->tableAlias.$this->realField");
          $this->query->addCondition($this->options['group'], $condition);
        }
        else {
          $where = '(' . $where . " OR $this->tableAlias.$this->realField IS NULL)";
        }
      }
      elseif ($this->view->getDatabaseDriver() == 'mongodb') {
          $condition = $this->view->getDatabaseCondition('AND');
          $condition->condition("$this->tableAlias.$this->realField", '', self::EQUAL);
          $this->query->addCondition($this->options['group'], $condition);
      }
    }
    else {
      if ($this->view->getDatabaseDriver() == 'mongodb') {
        $condition = $this->view->getDatabaseCondition('AND');
        $condition->condition("$this->tableAlias.$this->realField", '', self::NOT_EQUAL);
        $this->query->addCondition($this->options['group'], $condition);
      }
      else {
        $where .= "<> ''";
      }
    }

    if ($this->view->getDatabaseDriver() != 'mongodb') {
      $this->query->addWhereExpression($this->options['group'], $where);
    }
  }

}
