<?php

namespace Drupal\views\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;

/**
 * Filter handler which allows to search on multiple fields.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("combine")
 */
class Combine extends StringFilter {

  /**
   * @var views_plugin_query_default
   */
  public $query;

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['fields'] = ['default' => []];

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $this->view->initStyle();

    // Allow to choose all fields as possible
    if ($this->view->style_plugin->usesFields()) {
      $options = [];
      foreach ($this->view->display_handler->getHandlers('field') as $name => $field) {
        // Only allow clickSortable fields. Fields without clickSorting will
        // probably break in the Combine filter.
        if ($field->clickSortable()) {
          $options[$name] = $field->adminLabel(TRUE);
        }
      }
      if ($options) {
        $form['fields'] = [
          '#type' => 'select',
          '#title' => $this->t('Choose fields to combine for filtering'),
          '#description' => $this->t("This filter doesn't work for very special field handlers."),
          '#multiple' => TRUE,
          '#options' => $options,
          '#default_value' => $this->options['fields'],
        ];
      }
      else {
        $form_state->setErrorByName('', $this->t('You have to add some fields to be able to use this filter.'));
      }
    }
  }

  public function query() {
    $this->view->_build('field');
    $fields = [];
    $integer_fields = [];
    // Only add the fields if they have a proper field and table alias.
    foreach ($this->options['fields'] as $id) {
      // Overridden fields can lead to fields missing from a display that are
      // still set in the non-overridden combined filter.
      if (!isset($this->view->field[$id])) {
        // If fields are no longer available that are needed to filter by, make
        // sure no results are shown to prevent displaying more then intended.
        $this->view->build_info['fail'] = TRUE;
        continue;
      }
      $field = $this->view->field[$id];
      // Always add the table of the selected fields to be sure a table alias exists.
      $field->ensureMyTable();
      if (!empty($field->tableAlias) && !empty($field->realField)) {
        $fields[] = "$field->tableAlias.$field->realField";
      }

      if (isset($field->definition['id']) && ($field->definition['id'] == 'numeric') && (!isset($field->definition['float']) || !$field->definition['float'])  && !empty($field->realField)) {
        $integer_fields[] = '$' . $field->realField;
      }
    }
    if ($fields) {
      $base_table = $this->view->storage->get('base_table') . '.';
      $base_table_length = strlen($base_table);
      $count = count($fields);
      $separated_fields = [];
      foreach ($fields as $key => $field) {
        if ($this->view->getDatabaseDriver() == 'mongodb') {
          // MongoDB: Remove the base table name with dot from the field name.
          if (substr($field, 0, $base_table_length) === $base_table) {
            $field = substr($field, $base_table_length);
          }
          // MongoDB: For the $concat operator must the field names start with
          // the dollar sign.
          $separated_fields[] = '$' . $field;

          if ($key < $count - 1) {
            $separated_fields[] = ' ';
          }
        }
        else {
          $separated_fields[] = $field;

          if ($key < $count - 1) {
            $separated_fields[] = "' '";
          }
        }
      }
      if ($this->view->getDatabaseDriver() != 'mongodb') {
        $expression = implode(', ', $separated_fields);
        $expression = "CONCAT_WS(' ', $expression)";
      }

      $info = $this->operators();
      if (!empty($info[$this->operator]['method'])) {
        if ($this->view->getDatabaseDriver() == 'mongodb') {
          $values = [
            'separated fields' => $separated_fields,
            'integer fields' => $integer_fields
          ];
        }
        else {
          $values = [
            'expression' => $expression
          ];
        }
        $this->{$info[$this->operator]['method']}($values);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    if ($this->displayHandler->usesFields()) {
      $fields = $this->displayHandler->getHandlers('field');
      foreach ($this->options['fields'] as $id) {
        if (!isset($fields[$id])) {
          // Combined field filter only works with fields that are in the field
          // settings.
          $errors[] = $this->t('Field %field set in %filter is not set in display %display.', ['%field' => $id, '%filter' => $this->adminLabel(), '%display' => $this->displayHandler->display['display_title']]);
          break;
        }
        elseif (!$fields[$id]->clickSortable()) {
          // Combined field filter only works with simple fields. If the field
          // is not click sortable we can assume it is not a simple field.
          // @todo change this check to isComputed. See
          // https://www.drupal.org/node/2349465
          $errors[] = $this->t('Field %field set in %filter is not usable for this filter type. Combined field filter only works for simple fields.', ['%field' => $fields[$id]->adminLabel(), '%filter' => $this->adminLabel()]);
        }
      }
    }
    else {
      $errors[] = $this->t('%display: %filter can only be used on displays that use fields. Set the style or row format for that display to one using fields to use the combine field filter.', ['%display' => $this->displayHandler->display['display_title'], '%filter' => $this->adminLabel()]);
    }
    return $errors;
  }

  /**
   * By default things like opEqual uses add_where, that doesn't support
   * complex expressions, so override opEqual (and all operators below).
   */
  public function opEqual($values) {
    $placeholder = $this->placeholder();
    $operator = $this->operator();
    $expression = $values['expression'];
    $this->query->addWhereExpression($this->options['group'], "$expression $operator $placeholder", [$placeholder => $this->value]);
  }

  protected function opContains($values) {
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = [];
      foreach ($values['separated fields'] as $separated_field) {
        if (strpos($separated_field, '.') !== FALSE) {
          $placeholder = $this->placeholder();
          $this->query->addConditionField($placeholder, substr($separated_field, 1));
          $separated_fields[] = '$' . $placeholder;
        }
        else {
          $separated_fields[] = $separated_field;
        }
      }

      $placeholder = $this->placeholder();
      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      $condition->condition($placeholder, '%' . db_like($this->value) . '%', 'LIKE');
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $placeholder = $this->placeholder();
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression LIKE $placeholder", [$placeholder => '%' . db_like($this->value) . '%']);
    }
  }

  /**
   * Filters by one or more words.
   *
   * By default opContainsWord uses add_where, that doesn't support complex
   * expressions.
   *
   * @param string $expression
   */
  protected function opContainsWord($values) {
    $placeholder = $this->placeholder();

    // Don't filter on empty strings.
    if (empty($this->value)) {
      return;
    }

    // Match all words separated by spaces or sentences encapsulated by double
    // quotes.
    preg_match_all(static::WORDS_PATTERN, ' ' . $this->value, $matches, PREG_SET_ORDER);

    // Switch between the 'word' and 'allwords' operator.
    $type = $this->operator == 'word' ? 'OR' : 'AND';
    $group = $this->query->setWhereGroup($type);
    $operator = Database::getConnection()->mapConditionOperator('LIKE');
    $operator = isset($operator['operator']) ? $operator['operator'] : 'LIKE';

    if (($this->view->getDatabaseDriver() == 'mongodb') && !empty($matches)) {
      $separated_fields = $values['separated fields'];
      $this->query->addConcatField($placeholder, $separated_fields, $values['integer fields']);
      $condition = $this->view->getDatabaseCondition($type);
    }

    foreach ($matches as $match_key => $match) {
      $temp_placeholder = $placeholder . '_' . $match_key;
      // Clean up the user input and remove the sentence delimiters.
      $word = trim($match[2], ',?!();:-"');
      if ($this->view->getDatabaseDriver() == 'mongodb') {
        $condition->condition($placeholder, '%' . db_like($word) . '%', 'LIKE');
      }
      else {
        $expression = $values['expression'];
        $this->query->addWhereExpression($group, "$expression $operator $temp_placeholder", [$temp_placeholder => '%' . Database::getConnection()->escapeLike($word) . '%']);
      }
    }

    if (($this->view->getDatabaseDriver() == 'mongodb') && !empty($matches)) {
      $this->query->addCondition($group, $condition);
    }
  }

  protected function opStartsWith($values) {
    $placeholder = $this->placeholder();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = $values['separated fields'];

      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      $condition->condition($placeholder, db_like($this->value) . '%', 'LIKE');
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression LIKE $placeholder", [$placeholder => db_like($this->value) . '%']);
    }
  }

  protected function opNotStartsWith($values) {
    $placeholder = $this->placeholder();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = $values['separated fields'];

      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      $condition->condition($placeholder, db_like($this->value) . '%', 'NOT LIKE');
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression NOT LIKE $placeholder", [$placeholder => db_like($this->value) . '%']);
    }
  }

  protected function opEndsWith($values) {
    $placeholder = $this->placeholder();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = $values['separated fields'];

      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      $condition->condition($placeholder, '%' . db_like($this->value), 'LIKE');
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression LIKE $placeholder", [$placeholder => '%' . db_like($this->value)]);
    }
  }

  protected function opNotEndsWith($values) {
    $placeholder = $this->placeholder();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = $values['separated fields'];

      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      $condition->condition($placeholder, '%' . db_like($this->value), 'NOT LIKE');
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression NOT LIKE $placeholder", [$placeholder => '%' . db_like($this->value)]);
    }
  }

  protected function opNotLike($values) {
    $placeholder = $this->placeholder();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = $values['separated fields'];

      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      $condition->condition($placeholder, '%' . db_like($this->value) . '%', 'NOT LIKE');
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression NOT LIKE $placeholder", [$placeholder => '%' . db_like($this->value) . '%']);
    }
  }

  protected function opRegex($values) {
    $placeholder = $this->placeholder();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = $values['separated fields'];

      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      $condition->condition($placeholder, $this->value, 'REGEXP');
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression REGEXP $placeholder", [$placeholder => $this->value]);
    }
  }

  protected function opEmpty($values) {
    if ($this->operator == 'empty') {
      $operator = "IS NULL";
    }
    else {
      $operator = "IS NOT NULL";
    }

    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $separated_fields = $values['separated fields'];

      $this->query->addConcatField($placeholder, $separated_fields);
      $condition = $this->view->getDatabaseCondition('AND');
      // MongoDB: this is not going to work needs testing. Spaces between fields
      // are by definition not null.
      $condition->condition($placeholder, $operator);
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $expression = $values['expression'];
      $this->query->addWhereExpression($this->options['group'], "$expression $operator");
    }
  }

}
