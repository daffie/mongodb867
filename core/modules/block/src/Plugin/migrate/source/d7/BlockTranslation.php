<?php

namespace Drupal\block\Plugin\migrate\source\d7;

use Drupal\block\Plugin\migrate\source\Block;
use MongoDB\BSON\Binary;

/**
 * Gets i18n block data from source database.
 *
 * @MigrateSource(
 *   id = "d7_block_translation",
 *   source_module = "i18n_block"
 * )
 */
class BlockTranslation extends Block {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Let the parent set the block table to use, but do not use the parent
    // query. Instead build a query so can use an inner join to the selected
    // block table.
    parent::query();
    $query = $this->select('i18n_string', 'i18n')
      ->fields('i18n')
      ->fields('b', [
        'bid',
        'module',
        'delta',
        'theme',
        'status',
        'weight',
        'region',
        'custom',
        'visibility',
        'pages',
        'title',
        'cache',
        'i18n_mode',
      ])
      ->fields('lt', [
        'lid',
        'translation',
        'language',
        'plid',
        'plural',
        'i18n_status',
      ]);

    $i18n_mode = '1';
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $block_table_i18n_mode_data = $this->getDatabase()->tableInformation()->getTableField($this->blockTable, 'i18n_mode');
      if (isset($block_table_i18n_mode_data['type']) && in_array($block_table_i18n_mode_data['type'], ['int', 'serial'])) {
        $i18n_mode = 1;
      }
    }

    $query->addMongodbJoin('LEFT', $this->blockTable, 'delta', 'i18n_string', 'objectid', '=', 'b', [['field' => 'i18n_mode', 'value' => $i18n_mode]]);
    $query->addMongodbJoin('LEFT', 'locales_target', 'lid', 'i18n_string', 'lid', '=', 'lt');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        foreach (['delta', 'bid', 'module', 'theme', 'status', 'weight', 'region', 'custom', 'visibility', 'pages', 'title', 'cache', 'i18n_mode', 'translation', 'language', 'plid', 'plural', 'i18n_status'] as $field_name) {
          if (isset($row[$field_name]) && is_array($row[$field_name])) {
            $row[$field_name] = reset($row[$field_name]);
          }
          if (isset($row['lt_' . $field_name])) {
            unset($row['lt_' . $field_name]);
          }
          if (isset($row['b_' . $field_name])) {
            unset($row['b_' . $field_name]);
          }
        }
        if (isset($row['translation']) && ($row['translation'] instanceof Binary)) {
          $row['translation'] = $row['translation']->getData();
        }
        $rows[] = $row;
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'bid' => $this->t('The block numeric identifier.'),
      'module' => $this->t('The module providing the block.'),
      'delta' => $this->t("The block's delta."),
      'theme' => $this->t('Which theme the block is placed in.'),
      'status' => $this->t('Block enabled status'),
      'weight' => $this->t('Block weight within region'),
      'region' => $this->t('Theme region within which the block is set'),
      'visibility' => $this->t('Visibility'),
      'pages' => $this->t('Pages list.'),
      'title' => $this->t('Block title.'),
      'cache' => $this->t('Cache rule.'),
      'i18n_mode' => $this->t('Multilingual mode'),
      'lid' => $this->t('Language string ID'),
      'textgroup' => $this->t('A module defined group of translations'),
      'context' => $this->t('Full string ID for quick search: type:objectid:property.'),
      'objectid' => $this->t('Object ID'),
      'type' => $this->t('Object type for this string'),
      'property' => $this->t('Object property for this string'),
      'objectindex' => $this->t('Integer value of Object ID'),
      'format' => $this->t('The {filter_format}.format of the string'),
      'translation' => $this->t('Translation'),
      'language' => $this->t('Language code'),
      'plid' => $this->t('Parent lid'),
      'plural' => $this->t('Plural index number'),
      'i18n_status' => $this->t('Translation needs update'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['delta']['type'] = 'string';
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['delta']['alias'] = 'b';
    }
    $ids['language']['type'] = 'string';
    return $ids;
  }

}
