<?php

namespace Drupal\field\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 6 field instances source from database.
 *
 * @MigrateSource(
 *   id = "d6_field_instance",
 *   source_module = "content"
 * )
 */
class FieldInstance extends DrupalSqlBase {

  protected $content_node_fields = ['type', 'required', 'multiple', 'db_storage', 'module', 'active', 'locked', 'global_settings', 'db_columns'];
//  protected $content_node_fields = ['type', 'required', 'multiple', 'db_storage', 'module', 'active', 'locked', 'global_settings'];

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('content_node_field_instance', 'cnfi')
      ->fields('cnfi', [
        'field_name',
        'type_name',
        'weight',
        'label',
        'widget_type',
        'widget_settings',
        'display_settings',
        'description',
        'widget_module',
        'widget_active'
      ]);
    if (isset($this->configuration['node_type'])) {
      $query->condition('cnfi.type_name', $this->configuration['node_type']);
    }
    $query->addMongodbJoin('INNER', 'content_node_field', 'field_name', 'content_node_field_instance', 'field_name', '=', 'cnf');
    $query->fields('cnf', $this->content_node_fields);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'field_name' => $this->t('The machine name of field.'),
      'type_name' => $this->t('Content type where this field is in use.'),
      'weight' => $this->t('Weight.'),
      'label' => $this->t('A name to show.'),
      'widget_type' => $this->t('Widget type.'),
      'widget_settings' => $this->t('Serialize data with widget settings.'),
      'display_settings' => $this->t('Serialize data with display settings.'),
      'description' => $this->t('A description of field.'),
      'widget_module' => $this->t('Module that implements widget.'),
      'widget_active' => $this->t('Status of widget'),
      'module' => $this->t('The module that provides the field.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      foreach ($this->content_node_fields as $content_node_field) {
        if ($value = $row->getSourceProperty('cnf_' . $content_node_field)) {
          if (is_array($value)) {
            $value = reset($value);
            $row->setSourceProperty($content_node_field, $value);
            $row->setSourceProperty('cnf_' . $content_node_field, '');
          }
        }
      }
    }

    // Unserialize data.
    $widget_settings = unserialize($row->getSourceProperty('widget_settings'));
    $display_settings = unserialize($row->getSourceProperty('display_settings'));
    $global_settings = unserialize($row->getSourceProperty('global_settings'));
    $row->setSourceProperty('widget_settings', $widget_settings);
    $row->setSourceProperty('display_settings', $display_settings);
    $row->setSourceProperty('global_settings', $global_settings);
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [
      'field_name' => [
        'type' => 'string',
      ],
      'type_name' => [
        'type' => 'string',
      ],
    ];
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['field_name']['alias'] = 'cnfi';
    }
    return $ids;
  }

}
