<?php

namespace Drupal\field\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use MongoDB\BSON\Binary;

/**
 * Gets field option label translations.
 *
 * @MigrateSource(
 *   id = "d6_field_option_translation",
 *   source_module = "i18ncck"
 * )
 */
class FieldOptionTranslation extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Get the fields that have field options translations.
    $query = $this->select('i18n_strings', 'i18n')
      ->fields('i18n')
      ->fields('lt', [
        'translation',
        'language',
        'plid',
        'plural',
        'i18n_status',
      ])
      ->condition('i18n.type', 'field')
      ->condition('property', 'option\_%', 'LIKE');
    $query->addMongodbJoin('LEFT', 'locales_target', 'lid', 'i18n_strings', 'lid', '=', 'lt', [['field' => 'translation', 'value' => '', 'operator' => '<>']]);
    $query->addMongodbJoin('LEFT', 'content_node_field', 'field_name', 'i18n_strings', 'objectid', '=', 'cnf');
    $query->addField('cnf', 'field_name');
    $query->addField('cnf', 'global_settings');
    // Minimise changes to the d6_field_option_translation.yml, which is copied
    // from d6_field.yml, by ensuring the 'type' property is from
    // content_node_field table.
    $query->addField('cnf', 'type');
    $query->addField('i18n', 'type', 'i18n_type');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'property' => $this->t('Option ID.'),
      'objectid' => $this->t('Object ID'),
      'objectindex' => $this->t('Integer value of Object ID'),
      'format' => $this->t('The input format used by this string'),
      'lid' => $this->t('Source string ID'),
      'language' => $this->t('Language code'),
      'translation' => $this->t('Translation of the option'),
      'plid' => $this->t('Parent lid'),
      'plural' => $this->t('Plural index number in case of plural strings'),
    ];
    return parent::fields() + $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      return [
        'field_name' => ['type' => 'string', 'alias' => 'cnf'],
        'language' => ['type' => 'string'],
        'property' => ['type' => 'string'],
      ];
    }
    else {
      return parent::getIds() +
        [
          'language' => ['type' => 'string'],
          'property' => ['type' => 'string'],
        ];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        foreach (['translation', 'language', 'plid', 'plural', 'i18n_status', 'type_name', 'timestamp', 'revision_uid', 'field_name', 'global_settings', 'cnf_type'] as $field_name) {
          if (isset($row[$field_name]) && is_array($row[$field_name])) {
            $row[$field_name] = reset($row[$field_name]);
          }
          if (isset($row['lt_' . $field_name])) {
            unset($row['lt_' . $field_name]);
          }
          if (isset($row['i18n_' . $field_name])) {
            unset($row['i18n_' . $field_name]);
          }
          if (isset($row['cnf_' . $field_name])) {
            unset($row['cnf_' . $field_name]);
          }
          if (isset($row['cnfi_' . $field_name])) {
            unset($row['cnfi_' . $field_name]);
          }
        }
        if (isset($row['cnf_type'])) {
          $row['type'] = $row['cnf_type'];
          unset($row['cnf_type']);
        }
        if ($row['translation'] instanceof Binary) {
          $row['translation'] = $row['translation']->getData();
        }

        $rows[] = $row;
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // The instance widget_type helps determine what D8 field type we'll use.
    // Identify the distinct widget_types being used in D6.
    $query = $this->select('content_node_field_instance', 'cnfi')
      ->fields('cnfi', ['widget_type'])
      ->condition('field_name', $row->getSourceProperty('field_name'))
      ->orderBy('widget_type');

    // MongoDB does not like distinct queries.
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $query->distinct();
    }

    $widget_types = $query->execute()->fetchCol();

    // For MongoDB remove the double values.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $non_double_widget_types = [];
      foreach ($widget_types as $widget_type) {
        if (!in_array($widget_type, $non_double_widget_types)) {
          $non_double_widget_types[] = $widget_type;
        }
      }
      $widget_types = $non_double_widget_types;
    }

    // Arbitrarily use the first widget_type - if there are multiples, let the
    // migrator know.
    $row->setSourceProperty('widget_type', $widget_types[0]);
    if (count($widget_types) > 1) {
      $this->migration->getIdMap()->saveMessage(
        ['field_name' => $row->getSourceProperty('field_name')],
        $this->t('Widget types @types are used in Drupal 6 field instances: widget type @selected_type applied to the Drupal 8 base field', [
          '@types' => implode(', ', $widget_types),
          '@selected_type' => $widget_types[0],
        ])
      );
    }

    // Unserialize data.
    $global_settings = unserialize($row->getSourceProperty('global_settings'));
    $db_columns = unserialize($row->getSourceProperty('db_columns'));
    $row->setSourceProperty('global_settings', $global_settings);
    $row->setSourceProperty('db_columns', $db_columns);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this
      ->initializeIterator()
      ->count();
  }

}
