<?php

namespace Drupal\field\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 field source from database.
 *
 * @internal
 *
 * This class is marked as internal and should not be extended. Use
 * Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase instead.
 *
 * @MigrateSource(
 *   id = "d7_field",
 *   source_module = "field_sql_storage"
 * )
 */
class Field extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('field_config', 'fc')
      ->fields('fc')
      ->fields('fci', ['entity_type']);

    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $table_info = $this->getDatabase()->tableInformation();
      $field_config_data = $table_info->getTableFields('field_config');
      $field_config_instance_deleted_data = $table_info->getTableField('field_config_instance', 'deleted');

      if (isset($field_config_data['active']['type']) && in_array($field_config_data['active']['type'], ['int', 'serial'])) {
        $query->condition('fc.active', 1);
      }
      else {
        $query->condition('fc.active', '1');
      }

      if (isset($field_config_data['storage_active']['type']) && in_array($field_config_data['storage_active']['type'], ['int', 'serial'])) {
        $query->condition('fc.storage_active', 1);
      }
      else {
        $query->condition('fc.storage_active', '1');
      }

      if (isset($field_config_data['deleted']['type']) && in_array($field_config_data['deleted']['type'], ['int', 'serial'])) {
        $query->condition('fc.deleted', 0);
      }
      else {
        $query->condition('fc.deleted', '0');
      }

      if (isset($field_config_instance_deleted_data['type']) && in_array($field_config_instance_deleted_data['type'], ['int', 'serial'])) {
        $deleted = 0;
      }
      else {
        $deleted = '0';
      }
    }
    else {
      $query->distinct();
      $query->condition('fc.active', '1');
      $query->condition('fc.storage_active', '1');
      $query->condition('fc.deleted', '0');
    }

    $query->addMongodbJoin('LEFT', 'field_config_instance', 'field_id', 'field_config', 'id', '=', 'fci', [['field' => 'deleted', 'value' => $deleted]]);

    // If the Drupal 7 Title module is enabled, we don't want to migrate the
    // fields it provides. The values of those fields will be migrated to the
    // base fields they were replacing.
    if ($this->moduleExists('title')) {
      $title_fields = [
        'title_field',
        'name_field',
        'description_field',
        'subject_field',
      ];
      $query->condition('field_name', $title_fields, 'NOT IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The field ID.'),
      'field_name' => $this->t('The field name.'),
      'type' => $this->t('The field type.'),
      'module' => $this->t('The module that implements the field type.'),
      'active' => $this->t('The field status.'),
      'storage_type' => $this->t('The field storage type.'),
      'storage_module' => $this->t('The module that implements the field storage type.'),
      'storage_active' => $this->t('The field storage status.'),
      'locked' => $this->t('Locked'),
      'data' => $this->t('The field data.'),
      'cardinality' => $this->t('Cardinality'),
      'translatable' => $this->t('Translatable'),
      'deleted' => $this->t('Deleted'),
      'instances' => $this->t('The field instances.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        if (isset($row['fci_entity_type'])) {
          $row['entity_type'] = $row['fci_entity_type'];
          unset($row['fci_entity_type']);
        }
        if (is_array($row['entity_type'])) {
          $entity_types = $row['entity_type'];
          foreach ($entity_types as $entity_type) {
            $row['entity_type'] = $entity_type;
            $rows[] = $row;
          }
        }
      }

      // Remove the double rows.
      $field_names_entity_types = [];
      foreach ($rows as $id => $row) {
        $double = FALSE;
        foreach ($field_names_entity_types as $field_name_entity_type) {
          if (($row['field_name'] == $field_name_entity_type['field_name']) && ($row['entity_type'] == $field_name_entity_type['entity_type'])) {
            $double = TRUE;
          }
        }
        if ($double) {
          unset($rows[$id]);
        }
        else {
          $field_names_entity_types[] = ['field_name' => $row['field_name'], 'entity_type' => $row['entity_type']];
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row, $keep = TRUE) {
    foreach (unserialize($row->getSourceProperty('data')) as $key => $value) {
      $row->setSourceProperty($key, $value);
    }

    $instances = $this->select('field_config_instance', 'fci')
      ->fields('fci')
      ->condition('field_name', $row->getSourceProperty('field_name'))
      ->condition('entity_type', $row->getSourceProperty('entity_type'))
      ->execute()
      // MongoDB returns rows as objects by default.
      ->fetchAll(\PDO::FETCH_ASSOC);
    $row->setSourceProperty('instances', $instances);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [
      'field_name' => [
        'type' => 'string',
      ],
      'entity_type' => [
        'type' => 'string',
        'alias' => 'fci',
      ],
    ];
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['field_name']['alias'] = 'fc';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      return $this->initializeIterator()->count();
    }
    else {
      return parent::count();
    }
  }

}
