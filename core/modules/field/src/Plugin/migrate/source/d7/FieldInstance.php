<?php

namespace Drupal\field\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 field instances source from database.
 *
 * @internal
 *
 * This class is marked as internal and should not be extended. Use
 * Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase instead.
 *
 * @MigrateSource(
 *   id = "d7_field_instance",
 *   source_module = "field"
 * )
 */
class FieldInstance extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('field_config_instance', 'fci')
      ->fields('fci')
      ->fields('fc', ['type', 'translatable']);

    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $table_info = $this->getDatabase()->tableInformation();
      $field_config_data = $table_info->getTableFields('field_config');
      $field_config_instance_deleted_data = $table_info->getTableField('field_config_instance', 'deleted');

      if (isset($field_config_instance_deleted_data['type']) && in_array($field_config_instance_deleted_data['type'], ['int', 'serial'])) {
        $query->condition('fci.deleted', 0);
      }
      else {
        $query->condition('fci.deleted', '0');
      }

      $extra = [];
      if (isset($field_config_data['active']['type']) && in_array($field_config_data['active']['type'], ['int', 'serial'])) {
        $extra[] = [
          'field' => 'active',
          'value' => 1
        ];
      }
      else {
        $extra[] = [
          'field' => 'active',
          'value' => '1'
        ];
      }

      if (isset($field_config_data['storage_active']['type']) && in_array($field_config_data['storage_active']['type'], ['int', 'serial'])) {
        $extra[] = [
          'field' => 'storage_active',
          'value' => 1
        ];
      }
      else {
        $extra[] = [
          'field' => 'storage_active',
          'value' => '1'
        ];
      }

      if (isset($field_config_data['deleted']['type']) && in_array($field_config_data['deleted']['type'], ['int', 'serial'])) {
        $extra[] = [
          'field' => 'deleted',
          'value' => 0
        ];
      }
      else {
        $extra[] = [
          'field' => 'deleted',
          'value' => '0'
        ];
      }
    }
    else {
      $query->condition('fci.deleted', '0');

      $extra = [
        [
          'field' => 'active',
          'value' => '1'
        ],
        [
          'field' => 'storage_active',
          'value' => '1'
        ],
        [
          'field' => 'deleted',
          'value' => '0'
        ]
      ];
    }

    // If the Drupal 7 Title module is enabled, we don't want to migrate the
    // fields it provides. The values of those fields will be migrated to the
    // base fields they were replacing.
    if ($this->moduleExists('title')) {
      $title_fields = [
        'title_field',
        'name_field',
        'description_field',
        'subject_field',
      ];
      if ($this->getDatabase()->databaseType() == 'mongodb') {
        foreach ($title_fields as $title_field) {
          $extra[] = [
            'field' => 'field_name',
            'value' => $title_field,
            'operator' => '<>',
          ];
        }
      }
      else {
        $query->condition('fc.field_name', $title_fields, 'NOT IN');
      }
    }

    $query->addMongodbJoin('INNER', 'field_config', 'id', 'field_config_instance', 'field_id', '=', 'fc', $extra);

    // Optionally filter by entity type and bundle.
    if (isset($this->configuration['entity_type'])) {
      $query->condition('entity_type', $this->configuration['entity_type']);

      if (isset($this->configuration['bundle'])) {
        $query->condition('bundle', $this->configuration['bundle']);
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $results = $this->prepareQuery()->execute()->fetchAll();

    // Group all instances by their base field.
    $instances = [];
    foreach ($results as $result) {
      foreach (['type', 'translatable'] as $field_name) {
        if (isset($result[$field_name]) && is_array($result[$field_name])) {
          $result[$field_name] = reset($result[$field_name]);
        }
        if (isset($result['fc_' . $field_name])) {
          unset($result['fc_' . $field_name]);
        }
      }

      $instances[$result['field_id']][] = $result;
    }

    // Add the array of all instances using the same base field to each row.
    $rows = [];
    foreach ($results as $result) {
      $result['instances'] = $instances[$result['field_id']];

      foreach (['type', 'translatable'] as $field_name) {
        if (isset($result[$field_name]) && is_array($result[$field_name])) {
          $result[$field_name] = reset($result[$field_name]);
        }
        if (isset($result['fc_' . $field_name])) {
          unset($result['fc_' . $field_name]);
        }
      }

      $rows[] = $result;
    }

    return new \ArrayIterator($rows);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The field instance ID.'),
      'field_id' => $this->t('The field ID.'),
      'field_name' => $this->t('The field name.'),
      'entity_type' => $this->t('The entity type.'),
      'bundle' => $this->t('The entity bundle.'),
      'data' => $this->t('The field instance data.'),
      'deleted' => $this->t('Deleted'),
      'type' => $this->t('The field type'),
      'instances' => $this->t('The field instances.'),
      'field_definition' => $this->t('The field definition.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    foreach (unserialize($row->getSourceProperty('data')) as $key => $value) {
      $row->setSourceProperty($key, $value);
    }

    $field_id = (string) $row->getSourceProperty('field_id');
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $field_config_id_data = $this->getDatabase()->tableInformation()->getTableField('field_config', 'id');
      if (isset($field_config_id_data['type']) && in_array($field_config_id_data['type'], ['int', 'serial'])) {
        $field_id = (int) $field_id;
      }
    }

    $field_definition = $this->select('field_config', 'fc')
      ->fields('fc')
      ->condition('id', $field_id)
      ->execute()
      ->fetch(\PDO::FETCH_ASSOC);
    $row->setSourceProperty('field_definition', $field_definition);

    $translatable = FALSE;
    if ($row->getSourceProperty('entity_type') == 'node') {
      $language_content_type_bundle = (int) $this->variableGet('language_content_type_' . $row->getSourceProperty('bundle'), 0);
      // language_content_type_[bundle] may be
      //   - 0: no language support
      //   - 1: language assignment support
      //   - 2: node translation support
      //   - 4: entity translation support
      if ($language_content_type_bundle === 2 || ($language_content_type_bundle === 4 && $row->getSourceProperty('translatable'))) {
        $translatable = TRUE;
      }
    }
    else {
      // This is not a node entity. Get the translatable value from the source
      // field_config table.
      $field_data = unserialize($field_definition['data']);
      $translatable = $field_data['translatable'];
    }
    $row->setSourceProperty('translatable', $translatable);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [
      'entity_type' => [
        'type' => 'string',
      ],
      'bundle' => [
        'type' => 'string',
      ],
      'field_name' => [
        'type' => 'string',
      ],
    ];
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['entity_type']['alias'] = 'fci';
      $ids['bundle']['alias'] = 'fci';
      $ids['field_name']['alias'] = 'fci';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return $this->initializeIterator()->count();
  }

}
