<?php

namespace Drupal\migrate_high_water_test\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * Source plugin for migration high water tests.
 *
 * @MigrateSource(
 *   id = "high_water_test"
 * )
 */
class HighWaterTest extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $field_names = array_keys($this->fields());
    $query = $this
      ->select('high_water_node', 'm')
      ->fields('m', $field_names);
    // Remove the group by part, because MongoDB does not like that in
    // combination with a join.
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      foreach ($field_names as $field_name) {
        $query->groupBy($field_name);
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('Id'),
      'title' => $this->t('Title'),
      'changed' => $this->t('Changed'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $ids_titles_changeds = [];
      $rows = [];
      foreach ($iterator as $key => $row) {
        $double = FALSE;
        foreach ($ids_titles_changeds as $id_title_changed) {
          if (($id_title_changed['id'] == $row['id']) && ($id_title_changed['title'] == $row['title']) && ($id_title_changed['changed'] == $row['changed'])) {
            $double = TRUE;
          }
        }

        if (!$double) {
          $id_title_changed[] = ['id' => $row['id'], 'title' => $row['title'], 'changed' => $row['changed']];
          $rows[] = $row;
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this
      ->initializeIterator()
      ->count();
  }

}
