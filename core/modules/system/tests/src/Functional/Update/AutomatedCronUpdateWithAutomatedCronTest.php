<?php

namespace Drupal\Tests\system\Functional\Update;

use Drupal\Core\Database\Database;
use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Ensures that the automated cron module is installed on update.
 *
 * @group Update
 * @group legacy
 */
class AutomatedCronUpdateWithAutomatedCronTest extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      __DIR__ . '/../../../../tests/fixtures/update/drupal-8.bare.standard.php.gz',
    ];
  }

  /**
   * Ensures that automated cron module isn installed and the config migrated.
   */
  public function testUpdate() {
    if (Database::getConnection()->databaseType() == 'mongodb') {
      $this->markTestSkipped('The database dump files are for relational databases and therefore do not work with MongoDB.');
    }

    $this->runUpdates();

    $module_data = \Drupal::config('core.extension')->get('module');
    $this->assertTrue(isset($module_data['automated_cron']), 'The automated cron module was installed.');
  }

}
