<?php

namespace Drupal\comment;

use Drupal\Component\Utility\NestedArray;
use Drupal\views\EntityViewsData;

/**
 * Provides views data for the comment entity type.
 */
class CommentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data = NestedArray::mergeDeep($data, comment_views_data_extra());

    return $data;
  }

}
