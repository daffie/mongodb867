<?php

namespace Drupal\Tests\content_translation\Kernel;

use Drupal\Core\Database\Database;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the content translation settings API.
 *
 * @group content_translation
 */
class ContentTranslationSettingsApiTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['language', 'content_translation', 'user', 'entity_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('entity_test_mul');
  }

  /**
   * Tests that enabling translation via the API triggers schema updates.
   */
  public function testSettingsApi() {
    $this->container->get('content_translation.manager')->setEnabled('entity_test_mul', 'entity_test_mul', TRUE);

    if (Database::getConnection()->databaseType() == 'mongodb') {
      // Translation tables are embedded tables in MongoDB. There is no data
      // inserted into these tables. We therefor must use the table information
      // to find out if the correct fields are created.
      $result =
        Database::getConnection()->tableInformation()->getTableField('entity_test_mul_translations', 'content_translation_source') &&
        Database::getConnection()->tableInformation()->getTableField('entity_test_mul_translations', 'content_translation_outdated');
    }
    else {
      $result =
        db_field_exists('entity_test_mul_property_data', 'content_translation_source') &&
        db_field_exists('entity_test_mul_property_data', 'content_translation_outdated');
    }
    $this->assertTrue($result, 'Schema updates correctly performed.');
  }

}
