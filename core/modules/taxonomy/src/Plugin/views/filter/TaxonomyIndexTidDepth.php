<?php

namespace Drupal\taxonomy\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Filter handler for taxonomy terms with depth.
 *
 * This handler is actually part of the node table and has some restrictions,
 * because it uses a subquery to find nodes with.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("taxonomy_index_tid_depth")
 */
class TaxonomyIndexTidDepth extends TaxonomyIndexTid {

  public function operatorOptions($which = 'title') {
    return [
      'or' => $this->t('Is one of'),
    ];
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['depth'] = ['default' => 0];

    return $options;
  }

  public function buildExtraOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildExtraOptionsForm($form, $form_state);

    $form['depth'] = [
      '#type' => 'weight',
      '#title' => $this->t('Depth'),
      '#default_value' => $this->options['depth'],
      '#description' => $this->t('The depth will match nodes tagged with terms in the hierarchy. For example, if you have the term "fruit" and a child term "apple", with a depth of 1 (or higher) then filtering for the term "fruit" will get nodes that are tagged with "apple" as well as "fruit". If negative, the reverse is true; searching for "apple" will also pick up nodes tagged with "fruit" if depth is -1 (or lower).'),
    ];
  }

  public function query() {
    // If no filter values are present, then do nothing.
    if (count($this->value) == 0) {
      return;
    }
    elseif (count($this->value) == 1) {
      // Sometimes $this->value is an array with a single element so convert it.
      if (is_array($this->value)) {
        $this->value = current($this->value);
      }
      $this->value = (int) $this->value;
      $operator = '=';
    }
    else {
      foreach ($this->value as &$value) {
        $value = (int) $value;
      }
      $operator = 'IN';
    }

    // The normal use of ensureMyTable() here breaks Views.
    // So instead we trick the filter into using the alias of the base table.
    //   See https://www.drupal.org/node/271833.
    // If a relationship is set, we must use the alias it provides.
    if (!empty($this->relationship)) {
      $this->tableAlias = $this->relationship;
    }
    // If no relationship, then use the alias of the base table.
    else {
      $this->tableAlias = $this->query->ensureTable($this->view->storage->get('base_table'));
    }

    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $condition = $this->view->getDatabaseCondition('OR');

      // depth 0
      $this->query->addUnrelatedJoin('LEFT', 'taxonomy_index', NULL, $this->tableAlias, NULL, NULL, 'ti0', [['field' => 'nid', 'left_field' => 'nid'], ['field' => 'tid', 'value' => $this->value, 'operator' => $operator]]);
      $condition->condition('ti0', NULL, 'ARRAY_NOT_EMPTY');

      if ($this->options['depth'] > 0) {
        // depth 1
        $this->query->addUnrelatedJoin('LEFT', 'taxonomy_term_data', NULL, $this->tableAlias, NULL, NULL, 'th11', [['field' => 'taxonomy_term_translations.taxonomy_term_translations__parent.parent_target_id', 'value' => $this->value, 'operator' => $operator]]);
        $this->query->addUnrelatedJoin('LEFT', 'taxonomy_index', 'tid', 'th11', 'tid', '=', 'ti1', [['field' => 'nid', 'left_field' => 'nid']]);
        $condition->condition('ti1', NULL, 'ARRAY_NOT_EMPTY');

        if ($this->options['depth'] > 1) {
          $abs_depth = abs($this->options['depth']);
          // Each depth level needs its own set of joins.
          foreach (range(2, $abs_depth) as $d) {
            // depth 2 and higher.
            $this->query->addUnrelatedJoin('LEFT', 'taxonomy_term_data', NULL, $this->tableAlias, NULL, NULL, 'th'.$d.'1', [['field' => 'taxonomy_term_translations.taxonomy_term_translations__parent.parent_target_id', 'value' => $this->value, 'operator' => $operator]]);
            foreach (range(1, ($abs_depth -1)) as $i) {
              $j = $i + 1;
              $this->query->addUnrelatedJoin('LEFT', 'taxonomy_term_data', 'taxonomy_term_translations.taxonomy_term_translations__parent.parent_target_id', 'th'.$d.$i, 'tid', '=', 'th'.$d.$j);
            }
            $this->query->addUnrelatedJoin('LEFT', 'taxonomy_index', 'tid', 'th'.$d.$abs_depth, 'tid', '=', 'ti'.$d, [['field' => 'nid', 'left_field' => 'nid']]);
            $condition->condition('ti'.$d, NULL, 'ARRAY_NOT_EMPTY');
          }
        }
      }
      elseif ($this->options['depth'] < 0) {
        // depth -1
        $this->query->addUnrelatedJoin('LEFT', 'taxonomy_term_data', NULL, $this->tableAlias, NULL, NULL, 'th11', [['field' => 'tid', 'value' => $this->value, 'operator' => $operator]]);
        $this->query->addUnrelatedJoin('LEFT', 'taxonomy_index', 'tid', 'th11', 'taxonomy_term_translations.taxonomy_term_translations__parent.parent_target_id', '=', 'ti1', [['field' => 'nid', 'left_field' => 'nid']]);
        $condition->condition('ti1', NULL, 'ARRAY_NOT_EMPTY');

        if ($this->options['depth'] < -1) {
          $abs_depth = abs($this->options['depth']);
          // Each depth level needs its own set of joins.
          foreach (range(2, $abs_depth) as $d) {
            // depth -2 and lower.
            $this->query->addUnrelatedJoin('LEFT', 'taxonomy_term_data', NULL, $this->tableAlias, NULL, NULL, 'th'.$d.'1', [['field' => 'tid', 'value' => $this->value, 'operator' => $operator]]);
            foreach (range(1, ($abs_depth -1)) as $i) {
              $j = $i + 1;
              $this->query->addUnrelatedJoin('LEFT', 'taxonomy_term_data', 'tid', 'th'.$d.$i, 'taxonomy_term_translations.taxonomy_term_translations__parent.parent_target_id', '=', 'th'.$d.$j);
            }
            $this->query->addUnrelatedJoin('LEFT', 'taxonomy_index', 'tid', 'th'.$d.$abs_depth, 'taxonomy_term_translations.taxonomy_term_translations__parent.parent_target_id', '=', 'ti'.$d, [['field' => 'nid', 'left_field' => 'nid']]);
            $condition->condition('ti'.$d, NULL, 'ARRAY_NOT_EMPTY');
          }
        }
      }

      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      // Now build the subqueries.
      $subquery = db_select('taxonomy_index', 'tn');
      $subquery->addField('tn', 'nid');
      $where = ($this->view->getDatabaseCondition('OR'))->condition('tn.tid', $this->value, $operator);
      $last = "tn";

      if ($this->options['depth'] > 0) {
        $subquery->leftJoin('taxonomy_term__parent', 'th', "th.entity_id = tn.tid");
        $last = "th";
        foreach (range(1, abs($this->options['depth'])) as $count) {
          $subquery->leftJoin('taxonomy_term__parent', "th$count", "$last.parent_target_id = th$count.entity_id");
          $where->condition("th$count.entity_id", $this->value, $operator);
          $last = "th$count";
        }
      }
      elseif ($this->options['depth'] < 0) {
        foreach (range(1, abs($this->options['depth'])) as $count) {
          $field = $count == 1 ? 'tid' : 'entity_id';
          $subquery->leftJoin('taxonomy_term__parent', "th$count", "$last.$field = th$count.parent_target_id");
          $where->condition("th$count.entity_id", $this->value, $operator);
          $last = "th$count";
        }
      }

      $subquery->condition($where);
      $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", $subquery, 'IN');
    }
  }

}
