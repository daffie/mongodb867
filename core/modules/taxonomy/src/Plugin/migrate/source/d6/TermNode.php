<?php

namespace Drupal\taxonomy\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Source returning tids from the term_node table for the current revision.
 *
 * @MigrateSource(
 *   id = "d6_term_node",
 *   source_module = "taxonomy"
 * )
 */
class TermNode extends DrupalSqlBase {

  /**
   * The join options between the node and the term node table.
   */
  const JOIN = [
    'field' => 'vid',
    'left_field' => 'vid',
    'extra' => []
  ];

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('term_node', 'tn')
      ->fields('tn', ['nid', 'vid'])
      ->fields('n', ['type']);

    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $query->distinct();
    }

    $vid = (string) $this->configuration['vid'];
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $term_data_vid_data = $this->getDatabase()->tableInformation()->getTableField('term_data', 'vid');
      if (isset($term_data_vid_data['type']) && in_array($term_data_vid_data['type'], ['int', 'serial'])) {
        $vid = (int) $this->configuration['vid'];
      }
    }

    // Because this is an inner join it enforces the current revision.
    $query->addMongodbJoin('INNER', 'term_data', 'tid', 'term_node', 'tid', '=', 'td', [['field' => 'vid', 'value' => $vid]]);
    $query->addMongodbJoin('INNER', 'node', static::JOIN['field'], 'term_node', static::JOIN['left_field'], '=', 'n', static::JOIN['extra']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'nid' => $this->t('The node revision ID.'),
      'vid' => $this->t('The node revision ID.'),
      'tid' => $this->t('The term ID.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $nids_vids = [];
      $rows = [];
      foreach ($iterator as $key => $row) {
        if (isset($row['n_type']) && is_array($row['n_type'])) {
          $row['type'] = reset($row['n_type']);
          unset($row['n_type']);
        }

        $double = FALSE;
        foreach ($nids_vids as $nid_vid) {
          if (($nid_vid['nid'] == $row['nid']) && ($nid_vid['vid'] == $row['vid'])) {
            $double = TRUE;
          }
        }

        if (!$double) {
          $nids_vids[] = ['nid' => $row['nid'], 'vid' => $row['vid']];
          $rows[] = $row;
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if ($type = $row->getSourceProperty('type')) {
      if (is_array($type)) {
        $type = reset($type);
        $row->setSourceProperty('type', $type);
      }
    }

    $nid = (string) $row->getSourceProperty('nid');
    $vid = (string) $this->configuration['vid'];
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $table_info = $this->getDatabase()->tableInformation();
      $term_node_nid_data = $table_info->getTableField('term_node', 'nid');
      $term_data_vid_data = $table_info->getTableField('term_data', 'vid');

      if (isset($term_node_nid_data['type']) && in_array($term_node_nid_data['type'], ['int', 'serial'])) {
        $nid = (int) $nid;
      }

      if (isset($term_data_vid_data['type']) && in_array($term_data_vid_data['type'], ['int', 'serial'])) {
        $vid = (int) $vid;
      }
    }

    // Select the terms belonging to the revision selected.
    $query = $this->select('term_node', 'tn')
      ->fields('tn', ['tid']);

    $extra = static::JOIN['extra'];
    $extra[] = ['field' => 'nid', 'value' => $nid];
    $query->addMongodbJoin('INNER', 'node', static::JOIN['field'], 'term_node', static::JOIN['left_field'], '=', 'n', $extra);
    $query->addMongodbJoin('INNER', 'term_data', 'tid', 'term_node', 'tid', '=', 'td', [['field' => 'vid', 'value' => $vid]]);
    $row->setSourceProperty('tid', $query->execute()->fetchCol());

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['vid']['type'] = 'integer';
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['vid']['alias'] = 'tn';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    $rows = $this->query()->execute()->fetchAll();

    // We need to remove the double rows beore counting.
    $nids_vids = [];
    foreach ($rows as $row) {
      $double = FALSE;
      foreach ($nids_vids as $nid_vid) {
        if (($nid_vid['nid'] == $row['nid']) && ($nid_vid['vid'] == $row['vid'])) {
          $double = TRUE;
        }
      }

      if (!$double) {
        $nids_vids[] = ['nid' => $row['nid'], 'vid' => $row['vid']];
      }
    }

    return count($nids_vids);
  }

}
