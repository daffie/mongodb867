<?php

namespace Drupal\taxonomy\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Taxonomy term source from database.
 *
 * @todo Support term_relation, term_synonym table if possible.
 *
 * @MigrateSource(
 *   id = "d6_taxonomy_term",
 *   source_module = "taxonomy"
 * )
 */
class Term extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('term_data', 'td')
      ->fields('td')
      ->orderBy('td.tid');

    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $query->distinct();
    }

    if (isset($this->configuration['bundle'])) {
      $bundle = (array) $this->configuration['bundle'];
      foreach ($bundle as &$b) {
        $b = (string) $b;
      }
      if ($this->getDatabase()->databaseType() == 'mongodb') {
        $term_data_bundle_data = $this->getDatabase()->tableInformation()->getTableField('term_data', 'bundle');
        if (isset($term_data_bundle_data['type']) && in_array($term_data_bundle_data['type'], ['int', 'serial'])) {
          foreach ($bundle as &$b) {
            $b = (int) $b;
          }
        }
      }

      $query->condition('td.vid', $bundle, 'IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      $tids = [];
      $tids_lids = [];
      foreach ($iterator as $key => $row) {
        if (isset($row['translation']) && is_array($row['translation'])) {
          $row['translation'] = reset($row['translation']);
        }
        if (isset($row['lt_translation'])) {
          unset($row['lt_translation']);
        }
        if (isset($row['lt'])) {
          unset($row['lt']);
        }
        if (isset($row['lt.language'])) {
          $row['ltlanguage'] = $row['lt.language'];
          $row['language'] = $row['lt.language'];
          unset($row['lt.language']);
        }

        // The implementation of $query->isNotNull('i18n.lid') and
        // $query->distinct();
        if (($this->getPluginId() == 'd6_term_localized_translation') && !empty($row['tid']) && !empty($row['lid'])) {
          $double = FALSE;
          foreach ($tids_lids as $tid_lid) {
            if (($tid_lid['tid'] == $row['tid']) && ($tid_lid['lid'] == $row['lid'])) {
              $double = TRUE;
            }
          }

          if (!$double) {
            $tids_lids[] = ['tid' => $row['tid'], 'lid' => $row['lid']];
            $rows[] = $row;
          }
        }
        elseif (($this->getPluginId() == 'd6_taxonomy_term') && !empty($row['tid']) && !in_array($row['tid'], $tids)) {
          $tids[] = $row['tid'];
          $rows[] = $row;
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'tid' => $this->t('The term ID.'),
      'vid' => $this->t('Existing term VID'),
      'name' => $this->t('The name of the term.'),
      'description' => $this->t('The term description.'),
      'weight' => $this->t('Weight'),
      'parent' => $this->t("The Drupal term IDs of the term's parents."),
    ];
    if (isset($this->configuration['translations'])) {
      $fields['language'] = $this->t('The term language.');
      $fields['trid'] = $this->t('Translation ID.');
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $tid = (string) $row->getSourceProperty('tid');
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $term_hierarchy_tid_data = $this->getDatabase()->tableInformation()->getTableField('term_hierarchy', 'tid');
      if (isset($term_hierarchy_tid_data['type']) && in_array($term_hierarchy_tid_data['type'], ['int', 'serial'])) {
        $tid = (int) $tid;
      }
    }

    // Find parents for this row.
    $parents = $this->select('term_hierarchy', 'th')
      ->fields('th', ['parent'])
      ->condition('tid', $tid)
      ->execute()
      ->fetchCol();
    foreach ($parents as &$parent) {
      $parent = (int) $parent;
    }
    $row->setSourceProperty('parent', $parents);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['tid']['type'] = 'integer';
    return $ids;
  }


  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this
      ->initializeIterator()
      ->count();
  }

}
