<?php

namespace Drupal\taxonomy\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use MongoDB\BSON\Binary;

/**
 * Drupal 6 vocabulary translations from source database.
 *
 * @MigrateSource(
 *   id = "d6_taxonomy_vocabulary_translation",
 *   source_module = "i18ntaxonomy"
 * )
 */
class VocabularyTranslation extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('vocabulary', 'v')
      ->fields('v', ['vid', 'name', 'description'])
      ->fields('i18n', ['lid', 'type', 'property', 'objectid'])
      ->fields('lt', ['lid', 'translation'])
      ->condition('i18n.type', 'vocabulary');
    $query->addField('lt', 'language', 'language');
    // The i18n_strings table has two columns containing the object ID, objectid
    // and objectindex. The objectid column is a text field. Therefore, for the
    // join to work in PostgreSQL, use the objectindex field as this is numeric
    // like the vid field.
    $query->addMongodbJoin('INNER', 'i18n_strings', 'objectindex', 'vocabulary', 'vid', '=', 'i18n');
    $query->addMongodbJoin('LEFT', 'locales_target', 'lid', 'i18n_strings', 'lid', '=', 'lt');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        foreach (['lid', 'type', 'property', 'objectid'] as $field) {
          $i18n_field = 'i18n_' . $field;
          if (isset($row[$i18n_field])) {
            if (is_array($row[$i18n_field])) {
              $row[$field] = reset($row[$i18n_field]);
            }
            else {
              $row[$field] = $row[$i18n_field];
            }
            unset($row[$i18n_field]);
          }
        }
        foreach (['lt_lid', 'translation', 'language'] as $field) {
          $lt_field = 'lt_' . $field;
          if (isset($row[$lt_field])) {
            if (is_array($row[$lt_field])) {
              $row[$field] = reset($row[$lt_field]);
            }
            else {
              $row[$field] = $row[$lt_field];
            }
            unset($row[$lt_field]);
          }
        }

        $rows[] = $row;
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'vid' => $this->t('The vocabulary ID.'),
      'language' => $this->t('Language for this field.'),
      'property' => $this->t('Name of property being translated.'),
      'translation' => $this->t('Translation of either the title or explanation.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['vid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      if ($translation = $row->getSourceProperty('translation')) {
        if (is_array($translation)) {
          $translation = reset($translation);
        }
        if ($translation instanceof Binary) {
          $translation = $translation->getData();
        }
        $row->setSourceProperty('translation', $translation);
      }

      if ($language = $row->getSourceProperty('language')) {
        if (is_array($language)) {
          $language = reset($language);
        }
        if (is_array($language) && empty($language)) {
          $language = '';
        }
        $row->setSourceProperty('language', $language);
      }
      else {
        $row->setSourceProperty('language', '');
      }

      if ($lt_lid = $row->getSourceProperty('lt_lid')) {
        if (is_array($lt_lid)) {
          $lt_lid = reset($lt_lid);
          $row->setSourceProperty('lt_lid', $lt_lid);
        }
      }
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      return $this->initializeIterator()->count();
    }
    else {
      return parent::count();
    }
  }

}
