<?php

namespace Drupal\filter\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\migrate\Row;
use MongoDB\BSON\Binary;

/**
 * Drupal 7 filter source from database.
 *
 * @MigrateSource(
 *   id = "d7_filter_format",
 *   source_module = "filter"
 * )
 */
class FilterFormat extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('filter_format', 'f')->fields('f');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'format' => $this->t('Format ID.'),
      'name' => $this->t('The name of the format.'),
      'cache' => $this->t('Whether the format is cacheable.'),
      'status' => $this->t('The status of the format'),
      'weight' => $this->t('The weight of the format'),
      'filters' => $this->t('The filters configured for the format.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $format = (string) $row->getSourceProperty('format');
    $status = '1';
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $table_info = $this->getDatabase()->tableInformation();
      $filter_format_data = $table_info->getTableField('filter', 'format');
      if (isset($filter_format_data['type']) && in_array($filter_format_data['type'], ['int', 'serial'])) {
        $format = (int) $format;
      }

      $filter_status_data = $table_info->getTableField('filter', 'status');
      if (isset($filter_status_data['type']) && in_array($filter_status_data['type'], ['int', 'serial'])) {
        $status = 1;
      }
    }

    // Find filters for this format.
    $filters = $this->select('filter', 'f')
      ->fields('f')
      ->condition('format', $format)
      ->condition('status', $status)
      ->execute()
      ->fetchAllAssoc('name', \PDO::FETCH_ASSOC);

    foreach ($filters as $id => $filter) {
      if (isset($filter['settings']) && ($filter['settings'] instanceof Binary)) {
        $filters[$id]['settings'] = ($filter['settings'])->getData();
      }

      $filters[$id]['settings'] = unserialize($filter['settings']);
    }
    $row->setSourceProperty('filters', $filters);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['format']['type'] = 'string';
    return $ids;
  }

}
