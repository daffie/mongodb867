<?php

namespace Drupal\node\Plugin\migrate\source\d6;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "d6_node",
 *   source_module = "node"
 *
 * )
 */
class Node extends DrupalSqlBase {

  /**
   * The join options between the node and the node_revisions table.
   */
  const JOIN = [
    'operator' => '=',
    'extra' => []
  ];

  /**
   * The default filter format.
   *
   * @var string
   */
  protected $filterDefaultFormat;

  /**
   * Cached field and field instance definitions.
   *
   * @var array
   */
  protected $fieldInfo;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityManagerInterface $entity_manager, ModuleHandler $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('node_revisions', 'nr');
    $query->addMongodbJoin('INNER', 'node', 'vid', 'node_revisions', 'vid', static::JOIN['operator'], 'n', static::JOIN['extra']);
    $this->handleTranslations($query);

    $query->fields('n', [
        'nid',
        'type',
        'language',
        'status',
        'created',
        'changed',
        'comment',
        'promote',
        'moderate',
        'sticky',
        'tnid',
        'translate',
      ])
      ->fields('nr', [
        'title',
        'body',
        'teaser',
        'log',
        'timestamp',
        'format',
        'vid',
      ]);
    $query->addField('n', 'uid', 'node_uid');
    $query->addField('nr', 'uid', 'revision_uid');

    // If the content_translation module is enabled, get the source langcode
    // to fill the content_translation_source field.
    if ($this->moduleHandler->moduleExists('content_translation')) {
      $query->addMongodbJoin('LEFT', 'node', 'nid', 'node', 'n.ntid', '=', 'nt');
      $query->addField('nt', 'language', 'source_langcode');
    }

    if (isset($this->configuration['node_type'])) {
      $query->condition('n.type', $this->configuration['node_type']);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $this->filterDefaultFormat = $this->variableGet('filter_default_format', '1');

    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        foreach (['nid', 'type', 'language', 'status', 'created', 'changed', 'comment', 'promote', 'moderate', 'sticky', 'tnid', 'translate', 'node_uid'] as $field_name) {
          if (isset($row['n_' . $field_name])) {
            $row[$field_name] = $row['n_' . $field_name];
            unset($row['n_' . $field_name]);
          }
          if (isset($row[$field_name]) && is_array($row[$field_name])) {
            $row[$field_name] = reset($row[$field_name]);
          }
        }

        // Check whether or not we want translations.
        if (empty($this->configuration['translations'])) {
          // No translations: Yield untranslated nodes, or default translations.
          if (($row['tnid'] == '0') || ($row['tnid'] == $row['nid'])) {
            $rows[($this->getPluginId() == 'd6_node' ? $row['nid'] : $row['vid'])] = $row;
          }
        }
        else {
          // Translations: Yield only non-default translations.
          if (($row['tnid'] != '0') && ($row['tnid'] != $row['nid'])) {
            $rows[($this->getPluginId() == 'd6_node' ? $row['nid']: $row['vid'])] = $row;
          }
        }
      }
      ksort($rows);
      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'nid' => $this->t('Node ID'),
      'type' => $this->t('Type'),
      'title' => $this->t('Title'),
      'body' => $this->t('Body'),
      'format' => $this->t('Format'),
      'teaser' => $this->t('Teaser'),
      'node_uid' => $this->t('Node authored by (uid)'),
      'revision_uid' => $this->t('Revision authored by (uid)'),
      'created' => $this->t('Created timestamp'),
      'changed' => $this->t('Modified timestamp'),
      'status' => $this->t('Published'),
      'promote' => $this->t('Promoted to front page'),
      'sticky' => $this->t('Sticky at top of lists'),
      'revision' => $this->t('Create new revision'),
      'language' => $this->t('Language (fr, en, ...)'),
      'tnid' => $this->t('The translation set id for this node'),
      'timestamp' => $this->t('The timestamp the latest revision of this node was created.'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if (($this->getDatabase()->databaseType() == 'mongodb') && ($type = $row->getSourceProperty('type'))) {
      // Set source_langcode.
      if ($source_langcode = $row->getSourceProperty('source_langcode')) {
        if (is_array($sourceid1)) {
          $source_langcode = reset($source_langcode);
          $row->setSourceProperty('source_langcode', $source_langcode);
        }
      }
    }

    // format = 0 can happen when the body field is hidden. Set the format to 1
    // to avoid migration map issues (since the body field isn't used anyway).
    if ($row->getSourceProperty('format') === '0') {
      $row->setSourceProperty('format', $this->filterDefaultFormat);
    }

    if ($this->moduleExists('content') && $this->getModuleSchemaVersion('content') >= 6001) {
      foreach ($this->getFieldValues($row) as $field => $values) {
        $row->setSourceProperty($field, $values);
      }
    }

    // Make sure we always have a translation set.
    if ($row->getSourceProperty('tnid') == 0) {
      $row->setSourceProperty('tnid', $row->getSourceProperty('nid'));
    }

    return parent::prepareRow($row);
  }

  /**
   * Gets field values for a node.
   *
   * @param \Drupal\migrate\Row $node
   *   The node.
   *
   * @return array
   *   Field values, keyed by field name.
   */
  protected function getFieldValues(Row $node) {
    $values = [];
    foreach ($this->getFieldInfo($node->getSourceProperty('type')) as $field => $info) {
      $field_data = $this->getFieldData($info, $node);
      $values[$field] = $field_data;
    }
    return $values;
  }

  /**
   * Gets field and instance definitions from the database.
   *
   * @param string $node_type
   *   The node type for which to get field info.
   *
   * @return array
   *   Field and instance information for the node type, keyed by field name.
   */
  protected function getFieldInfo($node_type) {
    if (!isset($this->fieldInfo)) {
      $this->fieldInfo = [];

      // Query the database directly for all field info.
      $query = $this->select('content_node_field_instance', 'cnfi');
      $query->addMongodbJoin('INNER', 'content_node_field', 'field_name', 'content_node_field_instance', 'field_name', '=', 'cnf');
      $query->fields('cnfi', ['field_name', 'type_name', 'weight', 'label', 'widget_type', 'widget_module', 'widget_active', 'widget_settings', 'display_settings', 'description']);
      $content_node_fields = ['type', 'required', 'multiple', 'db_storage', 'module', 'active', 'locked', 'global_settings', 'db_columns'];
      $query->fields('cnf', $content_node_fields);

      foreach ($query->execute() as $field) {
        $field = (array) $field;
        $this->fieldInfo[$field['type_name']][$field['field_name']] = $field;
      }
      foreach ($this->fieldInfo as $type => $fields) {
        foreach ($fields as $field => $info) {
          foreach ($info as $property => $value) {
            // Remove the table alias from the property.
            if (substr($property, 0, 4) == 'cnf_') {
              $property = substr($property, 4);
            }
            // MongoDB joined query values are embedded in their join array.
            if (in_array($property, $content_node_fields) && is_array($value)) {
              $this->fieldInfo[$type][$field][$property] = reset($value);
            }
            if ($property == 'db_columns' || preg_match('/_settings$/', $property)) {
              if (is_array($value)) {
                $this->fieldInfo[$type][$field][$property] = unserialize(reset($value));
              }
              else {
                $this->fieldInfo[$type][$field][$property] = unserialize($value);
              }
            }
          }
        }
      }
    }

    return isset($this->fieldInfo[$node_type]) ? $this->fieldInfo[$node_type] : [];
  }

  /**
   * Retrieves raw field data for a node.
   *
   * @param array $field
   *   A field and instance definition from getFieldInfo().
   * @param \Drupal\migrate\Row $node
   *   The node.
   *
   * @return array
   *   The field values, keyed by delta.
   */
  protected function getFieldData(array $field, Row $node) {
    $field_table = 'content_' . $field['field_name'];
    $node_table = 'content_type_' . $node->getSourceProperty('type');

    /** @var \Drupal\Core\Database\Schema $db */
    $db = $this->getDatabase()->schema();

    if ($db->tableExists($field_table)) {
      if ($this->getDatabase()->databaseType() == 'mongodb') {
        $fields_data = $this->getDatabase()->tableInformation()->getTableFields($field_table);
      }
      $query = $this->select($field_table, 't');

      // If the delta column does not exist, add it as an expression to
      // normalize the query results.
      if ($db->fieldExists($field_table, 'delta')) {
        $query->addField('t', 'delta');
      }
      elseif ($this->getDatabase()->databaseType() == 'mongodb') {
        $query->addLiteralField('delta', 0);
      }
      else {
        $query->addExpression(0, 'delta');
      }
    }
    elseif ($db->tableExists($node_table)) {
      if ($this->getDatabase()->databaseType() == 'mongodb') {
        $fields_data = $this->getDatabase()->tableInformation()->getTableFields($node_table);
      }
      $query = $this->select($node_table, 't');

      // Every row should have a delta of 0.
      if ($this->getDatabase()->databaseType() == 'mongodb') {
        $query->addLiteralField('delta', 0);
      }
      else {
        $query->addExpression(0, 'delta');
      }
    }

    if (isset($query)) {
      $columns = array_keys($field['db_columns']);

      // Add every column in the field's schema.
      foreach ($columns as $column) {
        $query->addField('t', $field['field_name'] . '_' . $column, $column);
      }

      // This call to isNotNull() is a kludge which relies on the convention
      // that field schemas usually define their most important column first.
      // A better way would be to allow field plugins to alter the query
      // directly before it's run, but this will do for the time being.
      $query->isNotNull($field['field_name'] . '_' . $columns[0]);

      if (($this->getDatabase()->databaseType() == 'mongodb') && isset($fields_data['nid']['type']) && in_array($fields_data['nid']['type'], ['int', 'serial'])) {
        $query->condition('nid', (int) $node->getSourceProperty('nid'));
      }
      else {
        $query->condition('nid', $node->getSourceProperty('nid'));
      }

      if (($this->getDatabase()->databaseType() == 'mongodb') && isset($fields_data['vid']['type']) && in_array($fields_data['vid']['type'], ['int', 'serial'])) {
        $query->condition('vid', (int) $node->getSourceProperty('vid'));
      }
      else {
        $query->condition('vid', $node->getSourceProperty('vid'));
      }

      // Make sure that the query returns the results as arrays.
      return $query->execute()->fetchAllAssoc('delta', \PDO::FETCH_ASSOC);
    }
    else {
      return [];
    }
  }

  /**
   * Retrieves raw field data for a node.
   *
   * @deprecated in Drupal 8.2.x, to be removed in Drupal 9.0.x. Use
   *   getFieldData() instead.
   *
   * @param array $field
   *   A field and instance definition from getFieldInfo().
   * @param \Drupal\migrate\Row $node
   *   The node.
   *
   * @return array
   *   The field values, keyed by delta.
   */
  protected function getCckData(array $field, Row $node) {
    return $this->getFieldData($field, $node);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['nid']['type'] = 'integer';
    $ids['nid']['alias'] = 'n';
    return $ids;
  }

  /**
   * Adapt our query for translations.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The generated query.
   */
  protected function handleTranslations(SelectInterface $query) {
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      // Check whether or not we want translations.
      if (empty($this->configuration['translations'])) {
        // No translations: Yield untranslated nodes, or default translations.
        $condition_or = $this->getDatabase()->condition('OR');
        $condition_or->condition('n.tnid', 0, '=');
        $condition_or->condition('n.tnid', 'n.nid', '=');
        $query->condition($condition_or);
      }
      else {
        // Translations: Yield only non-default translations.
        $query->condition('n.tnid', [0, 'n.nid'], 'NOT IN');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this
      ->initializeIterator()
      ->count();
  }

}
