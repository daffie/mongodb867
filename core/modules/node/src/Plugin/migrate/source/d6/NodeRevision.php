<?php

namespace Drupal\node\Plugin\migrate\source\d6;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;

/**
 * Drupal 6 node revision source from database.
 *
 * @MigrateSource(
 *   id = "d6_node_revision",
 *   source_module = "node"
 * )
 */
class NodeRevision extends Node {

  /**
   * The join options between the node and the node_revisions_table.
   */
  const JOIN = [
    'operator' => '<>',
    'extra' => [
      [
        'field' => 'nid',
        'left_field' => 'nid',
      ]
    ]
  ];

  /**
   * {@inheritdoc}
   */
  public function fields() {
    // Use all the node fields plus the vid that identifies the version.
    return parent::fields() + [
      'vid' => t('The primary identifier for this version.'),
      'log' => $this->t('Revision Log message'),
      'timestamp' => $this->t('Revision timestamp'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['vid']['type'] = 'integer';
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['vid']['alias'] = 'nr';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  protected function handleTranslations(SelectInterface $query) {
    // @todo in https://www.drupal.org/node/2746541
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      if ($type = $row->getSourceProperty('type')) {
        if (is_array($type)) {
          $type = reset($type);
          $row->setSourceProperty('type', $type);
        }
      }

      foreach (['nid', 'language', 'status', 'created', 'changed', 'comment', 'promote', 'moderate', 'sticky', 'tnid', 'translate', 'node_uid'] as $field_name) {
        if ($field_value = $row->getSourceProperty($field_name)) {
          if (is_array($field_value)) {
            $field_value = reset($field_value);
            $row->setSourceProperty($field_name, $field_value);
          }
        }
      }
    }

    // format = 0 can happen when the body field is hidden. Set the format to 1
    // to avoid migration map issues (since the body field isn't used anyway).
    if ($row->getSourceProperty('format') === '0') {
      $row->setSourceProperty('format', $this->filterDefaultFormat);
    }

    if ($this->moduleExists('content') && $this->getModuleSchemaVersion('content') >= 6001) {
      foreach ($this->getFieldValues($row) as $field => $values) {
        $row->setSourceProperty($field, $values);
      }
    }

    // Make sure we always have a translation set.
    if ($row->getSourceProperty('tnid') == 0) {
      $row->setSourceProperty('tnid', $row->getSourceProperty('nid'));
    }

    return parent::prepareRow($row);
  }

}
