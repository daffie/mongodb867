<?php

namespace Drupal\node\Plugin\views\filter;

use Drupal\user\Plugin\views\filter\Name;
use Drupal\views\Views;

/**
 * Filter handler to check for revisions a certain user has created.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("node_uid_revision")
 */
class UidRevision extends Name {

  public function query($group_by = FALSE) {
    $this->ensureMyTable();

    $placeholder = $this->placeholder() . '[]';

    $args = array_values($this->value);

    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $def = [];
      $def['table'] = 'node';
      $def['field'] = 'nid';
      $def['left_table'] = 'node';
      $def['left_field'] = 'nid';
      if (!empty($args)) {
        foreach ($args as &$arg) {
          $arg = (int) $arg;
        }
        $def['extra'] = [
          [
            'field' => 'node_all_revisions.revision_uid',
            'value' => $args,
            'operator' => 'IN',
          ]
        ];
      }

      $join = Views::pluginManager('join')->createInstance('standard', $def);

      $this->alias = $this->query->addRelationship('user_revision', $join, 'node', $this->relationship);

      $condition = $this->view->getDatabaseCondition('OR');
      $condition->condition('user_revision', NULL, 'IS NOT NULL'); // nid 1,2
      if (!empty($args)) {
        $condition->condition('node_current_revision.uid', $args, 'IN'); // nid 1
      }
      $this->query->addCondition($this->options['group'], $condition);
    }
    else {
      $this->query->addWhereExpression($this->options['group'], "$this->tableAlias.uid IN($placeholder) OR
        ((SELECT COUNT(DISTINCT vid) FROM {node_revision} nr WHERE nr.revision_uid IN ($placeholder) AND nr.nid = $this->tableAlias.nid) > 0)", [$placeholder => $args],
        $args);
    }
  }

}
