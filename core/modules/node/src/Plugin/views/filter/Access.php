<?php

namespace Drupal\node\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter by node_access records.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("node_access")
 */
class Access extends FilterPluginBase {

  public function adminSummary() {}

  protected function operatorForm(&$form, FormStateInterface $form_state) {}

  public function canExpose() {
    return FALSE;
  }

  /**
   * See _node_access_where_sql() for a non-views query based implementation.
   */
  public function query() {
    $account = $this->view->getUser();
    if (!$account->hasPermission('bypass node access')) {
      $table = $this->ensureMyTable();
      $grants = $this->view->getDatabaseCondition('OR');
      foreach (node_access_grants('view', $account) as $realm => $gids) {
        foreach ($gids as $gid) {
          $grants->condition(($this->view->getDatabaseCondition('AND'))
            ->condition($table . '.gid', (int) $gid)
            ->condition($table . '.realm', $realm)
          );
        }
      }

      if ($this->view->getDatabaseDriver() == 'mongodb') {
        $this->query->addCondition('AND', $grants);
        $this->query->addCondition('AND', $table . '.grant_view', TRUE);
      }
      else {
        $this->query->addWhere('AND', $grants);
        $this->query->addWhere('AND', $table . '.grant_view', 1, '>=');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();

    $contexts[] = 'user.node_grants:view';

    return $contexts;
  }

}
