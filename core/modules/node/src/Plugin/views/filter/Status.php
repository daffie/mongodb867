<?php

namespace Drupal\node\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter by published status.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("node_status")
 */
class Status extends FilterPluginBase {

  public function adminSummary() {}

  protected function operatorForm(&$form, FormStateInterface $form_state) {}

  public function canExpose() {
    return FALSE;
  }

  public function query() {
    $table = $this->ensureMyTable();
    if ($this->view->getDatabaseDriver() == 'mongodb') {
      $field_uid = 'node_current_revision.uid';
      $field_status = 'node_current_revision.status';
      if ($table != $this->view->storage->get('base_table')) {
        $field_uid = $table . '.' . $field_uid;
        $field_status = $table . '.' . $field_status;
      }

      $and_condition = $this->view->getDatabaseCondition('AND');
      $and_condition->condition($field_uid, '***CURRENT_USER***');
      $and_condition->condition('***CURRENT_USER***', 0, '<>');
      $and_condition->condition('***VIEW_OWN_UNPUBLISHED_NODES***', 1);

      $or_condition = $this->view->getDatabaseCondition('OR');
      $or_condition->condition($and_condition);
      $or_condition->condition($field_status, TRUE);
      $or_condition->condition('***BYPASS_NODE_ACCESS***', 1);

      $this->query->addCondition($this->options['group'], $or_condition);
    }
    else {
      $this->query->addWhereExpression($this->options['group'], "$table.status = 1 OR ($table.uid = ***CURRENT_USER*** AND ***CURRENT_USER*** <> 0 AND ***VIEW_OWN_UNPUBLISHED_NODES*** = 1) OR ***BYPASS_NODE_ACCESS*** = 1");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();

    $contexts[] = 'user';

    return $contexts;
  }

}
