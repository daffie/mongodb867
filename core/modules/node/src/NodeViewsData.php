<?php

namespace Drupal\node;

use Drupal\Component\Utility\NestedArray;
use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the node entity type.
 */
class NodeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $extra = node_views_data_extra();

    $extra['node_field_revision']['vid'] = NestedArray::mergeDeep($extra['node_field_revision']['vid'], $data['node_field_revision']['vid']);

    $data = NestedArray::mergeDeep($data, $extra);

    return $data;
  }

}
