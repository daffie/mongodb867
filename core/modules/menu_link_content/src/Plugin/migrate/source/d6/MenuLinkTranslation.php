<?php

namespace Drupal\menu_link_content\Plugin\migrate\source\d6;

use Drupal\content_translation\Plugin\migrate\source\I18nQueryTrait;
use Drupal\migrate\Row;
use Drupal\menu_link_content\Plugin\migrate\source\MenuLink;

/**
 * Gets Menu link translations from source database.
 *
 * @MigrateSource(
 *   id = "d6_menu_link_translation",
 *   source_module = "i18nmenu"
 * )
 */
class MenuLinkTranslation extends MenuLink {

  use I18nQueryTrait;

  /**
   * Drupal 6 table names.
   */
  const I18N_STRING_TABLE = 'i18n_strings';

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Ideally, the query would return rows for each language for each menu link
    // with the translations for both the title and description or just the
    // title translation or just the description translation. That query quickly
    // became complex and would be difficult to maintain.
    // Therefore, build a query based on i18nstrings table where each row has
    // the translation for only one property, either title or description. The
    // method prepareRow() is then used to obtain the translation for the other
    // property.
    // The query starts with the same query as menu_link.
    $query = parent::query();

    if ($this->getDatabase()->databaseType() != 'mongodb') {
      // Add in the property, which is either title or description. Cast the mlid
      // to text so PostgreSQL can make the join.
      $query->addMongodbJoin('LEFT', static::I18N_STRING_TABLE, 'objectid', 'menu_links', 'mlid', '=', 'i18n');
      $query->isNotNull('i18n.lid');
      $query->addField('i18n', 'lid');
      $query->addField('i18n', 'property');

      // Add in the translation for the property.
      $query->addMongodbJoin('LEFT', 'locales_target', 'lid', static::I18N_STRING_TABLE, 'lid', '=', 'lt');
      $query->addField('lt', 'language');
      $query->addField('lt', 'translation');
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $i18n_string_table_objectid_data = $this->getDatabase()->tableInformation()->getTableField(static::I18N_STRING_TABLE, 'objectid');

      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        if (isset($i18n_string_table_objectid_data['type']) && in_array($i18n_string_table_objectid_data['type'], ['int', 'serial'])) {
          $mlid = (int) $row['mlid'];
        }
        else {
          $mlid = (string) $row['mlid'];
        }

        $query = $this->select(static::I18N_STRING_TABLE, 'i18n')
          ->fields('i18n', ['property'])
          ->fields('lt', ['lid', 'translation', 'language'])
          ->condition('i18n.objectid', $mlid);
        $query->addMongodbJoin('LEFT', 'locales_target', 'lid', static::I18N_STRING_TABLE, 'lid', '=', 'lt');
        $query->addFilterUnwindPath('lt');
        $results = $query->execute()->fetchAll();
        foreach ($results as $result) {
          $row = array_merge($row, $result);

          // The implementation of $query->isNotNull('i18n.lid')
          if (!empty($row['lid'])) {
            $rows[] = $row;
          }
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    parent::prepareRow($row);

    // Save the translation for this property.
    $property_in_row = $row->getSourceProperty('property');

    // Set the i18n string table for use in I18nQueryTrait.
    $this->i18nStringTable = static::I18N_STRING_TABLE;
    // Get the translation for the property not already in the row and save it
    // in the row.
    $property_not_in_row = ($property_in_row == 'title') ? 'description' : 'title';
    $result = $this->getPropertyNotInRowTranslation($row, $property_not_in_row, 'mlid', $this->idMap);
    foreach (['description_translated', 'title_translated'] as $field_name) {
      if ($field_value = $row->getSourceProperty($field_name)) {
        if (is_array($field_value)) {
          $row->setSourceProperty($field_name, reset($field_value));
        }
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'language' => $this->t('Language for this menu.'),
      'title_translated' => $this->t('Menu link title translation.'),
      'description_translated' => $this->t('Menu link description translation.'),
    ];
    return parent::fields() + $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['language']['type'] = 'string';
    return parent::getIds() + $ids;
  }

}
