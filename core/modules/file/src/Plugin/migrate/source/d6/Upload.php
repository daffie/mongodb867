<?php

namespace Drupal\file\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 6 upload source from database.
 *
 * @MigrateSource(
 *   id = "d6_upload",
 *   source_module = "upload"
 * )
 */
class Upload extends DrupalSqlBase {

  /**
   * The join options between the node and the upload table.
   */
  const JOIN = [
    'extra' => [
      [
        'field' => 'vid',
        'left_field' => 'vid',
      ]
    ]
  ];

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('upload', 'u')
      ->fields('u', ['nid', 'vid']);
    $query->addMongodbJoin('INNER', 'node', 'nid', 'upload', 'nid', '=', 'n', static::JOIN['extra']);
    $query->addField('n', 'type');
    $query->addField('n', 'language');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      if ($language = $row->getSourceProperty('language')) {
        if (is_array($language)) {
          $language = reset($language);
          $row->setSourceProperty('language', $language);
        }
      }
      else {
        $row->setSourceProperty('language', '');
      }

      if ($type = $row->getSourceProperty('type')) {
        if (is_array($type)) {
          $type = reset($type);
          $row->setSourceProperty('type', $type);
        }
      }
      else {
        $row->setSourceProperty('type', '');
      }
    }
    $query = $this->select('upload', 'u')
      ->fields('u', ['fid', 'description', 'list'])
      // Watch out! The variable "nid" is a string.
      ->condition('u.nid', $row->getSourceProperty('nid'))
      ->orderBy('u.weight');
    $query->addMongodbJoin('INNER', 'node', 'nid', 'upload', 'nid', '=', 'n', static::JOIN['extra']);
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    $row->setSourceProperty('upload', $result);
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'fid' => $this->t('The file Id.'),
      'nid' => $this->t('The node Id.'),
      'vid' => $this->t('The version Id.'),
      'type' => $this->t('The node type'),
      'language' => $this->t('The node language.'),
      'description' => $this->t('The file description.'),
      'list' => $this->t('Whether the list should be visible on the node page.'),
      'weight' => $this->t('The file weight.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['vid']['type'] = 'integer';
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['vid']['alias'] = 'u';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // For MongoDB need to values from the joined table set to the correct
    // property.
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      foreach ($iterator as $key => $row) {
        // Test for double.
        $new = TRUE;
        foreach ($rows as $r) {
          if (($row['nid'] == $r['nid']) && ($row['vid'] == $r['vid'])) {
            $new = FALSE;
          }
        }

        // Only add the row if it is not a double.
        if ($new) {
          if (isset($row['type']) && is_array($row['type'])) {
            $row['type'] = reset($row['type']);
          }
          if (isset($row['n_type']) && is_array($row['n_type'])) {
            $row['type'] = reset($row['n_type']);
          }
          $rows[] = $row;
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

}
