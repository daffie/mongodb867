<?php

namespace Drupal\user\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 6 profile fields values source.
 *
 * @MigrateSource(
 *   id = "d6_profile_field_values",
 *   source_module = "profile"
 * )
 */
class ProfileFieldValues extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('profile_values', 'pv')
      ->fields('pv', ['uid']);

    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $query->distinct();
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $iterator = parent::initializeIterator();
      $rows = [];
      $uids = [];
      foreach ($iterator as $key => $row) {
        if (!in_array($row['uid'], $uids)) {
          $uids[] = $row['uid'];
          $rows[] = $row;
        }
      }

      return new \ArrayIterator($rows);
    }
    else {
      return parent::initializeIterator();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $uid = (string) $row->getSourceProperty('uid');
    if ($this->getDatabase()->databaseType() == 'mongodb') {
      $profile_values_uid_data = $this->getDatabase()->tableInformation()->getTableField('profile_values', 'uid');
      if (isset($profile_values_uid_data['type']) && in_array($profile_values_uid_data['type'], ['int', 'serial'])) {
        $uid = (int) $uid;
      }
    }
    // Find profile values for this row.
    $query = $this->select('profile_values', 'pv')
      ->fields('pv', ['fid', 'value']);
    $query->addMongodbJoin('LEFT', 'profile_fields', 'fid', 'profile_values', 'fid', '=', 'pf');
    $query->fields('pf', ['name', 'type']);
    $query->condition('uid', $uid);
    $results = $query->execute();

    foreach ($results as $profile_value) {
      if (is_object($profile_value)) {
        $profile_value = (array) $profile_value;
      }
      if (is_array($profile_value['pf_name'])) {
        $profile_value['name'] = reset($profile_value['pf_name']);
      }
      if (is_array($profile_value['pf_type'])) {
        $profile_value['type'] = reset($profile_value['pf_type']);
      }

      // Check special case for date. We need to unserialize.
      if ($profile_value['type'] == 'date') {
        $date = unserialize($profile_value['value']);
        $date = date('Y-m-d', mktime(0, 0, 0, $date['month'], $date['day'], $date['year']));
        $row->setSourceProperty($profile_value['name'], ['value' => $date]);
      }
      elseif ($profile_value['type'] == 'list') {
        // Explode by newline and comma.
        $row->setSourceProperty($profile_value['name'], preg_split("/[\r\n,]+/", $profile_value['value']));
      }
      else {
        $row->setSourceProperty($profile_value['name'], [$profile_value['value']]);
      }
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'fid' => $this->t('Unique profile field ID.'),
      'uid' => $this->t('The user Id.'),
      'value' => $this->t('The value for this field.'),
    ];

    $query = $this->select('profile_values', 'pv')
      ->fields('pv', ['fid', 'value']);
    $query->addMongodbJoin('LEFT', 'profile_fields', 'fid', 'profile_values', 'fid', '=', 'pf');
    $query->fields('pf', ['name', 'title']);
    $results = $query->execute();
    foreach ($results as $profile) {
      $profile = (array) $profile;
      $fields[$profile['pf_name']] = $this->t($profile['pf_title']);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['uid']['type'] = 'integer';
    if ($this->getDatabase()->databaseType() != 'mongodb') {
      $ids['uid']['alias'] = 'pv';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this
      ->initializeIterator()
      ->count();
  }

}
