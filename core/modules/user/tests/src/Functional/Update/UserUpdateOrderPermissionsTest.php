<?php

namespace Drupal\Tests\user\Functional\Update;

use Drupal\Core\Database\Database;
use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Tests user permissions sort upgrade path.
 *
 * @group Update
 * @group legacy
 */
class UserUpdateOrderPermissionsTest extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      __DIR__ . '/../../../../../system/tests/fixtures/update/drupal-8-rc1.bare.standard.php.gz',
    ];
  }

  /**
   * Tests that permissions are ordered by machine name.
   */
  public function testPermissionsOrder() {
    if (Database::getConnection()->databaseType() == 'mongodb') {
      $this->markTestSkipped('The database dump files are for relational databases and therefore do not work with MongoDB.');
    }

    $authenticated = \Drupal::config('user.role.authenticated');
    $permissions = $authenticated->get('permissions');
    sort($permissions);
    $this->assertNotSame($permissions, $authenticated->get('permissions'));

    $this->runUpdates();
    $authenticated = \Drupal::config('user.role.authenticated');
    $this->assertSame($permissions, $authenticated->get('permissions'));
  }

}
