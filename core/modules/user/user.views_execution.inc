<?php

/**
 * @file
 * Provide views runtime hooks for user.module.
 */

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_substitutions().
 *
 * Allow replacement of current user ID so we can cache these queries.
 */
function user_views_query_substitutions(ViewExecutable $view) {
  // MongoDB stores user ids as integer values. For MongoDB is an integer not
  // the same as a string (4 <> '4').
  return ['***CURRENT_USER***' => (int) \Drupal::currentUser()->id()];
}
