<?php

namespace Drupal\aggregator;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Controller class for aggregators items.
 *
 * This extends the Drupal\Core\Entity\Sql\SqlContentEntityStorage class, adding
 * required special handling for feed item entities.
 */
class ItemStorage extends SqlContentEntityStorage implements ItemStorageInterface {

  use ItemStorageTrait;

}
