<?php

namespace Drupal\aggregator;

use Drupal\Component\Utility\NestedArray;
use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the aggregator feed entity type.
 */
class AggregatorFeedViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data = NestedArray::mergeDeep($data, aggregator_feed_views_data_extra());

    return $data;
  }

}
