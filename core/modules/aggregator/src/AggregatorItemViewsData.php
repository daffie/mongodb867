<?php

namespace Drupal\aggregator;

use Drupal\Component\Utility\NestedArray;
use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the aggregator item entity type.
 */
class AggregatorItemViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data = NestedArray::mergeDeep($data, aggregator_item_views_data_extra());

    return $data;
  }

}
