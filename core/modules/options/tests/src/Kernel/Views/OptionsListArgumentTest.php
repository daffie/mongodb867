<?php

namespace Drupal\Tests\options\Kernel\Views;

use Drupal\views\Views;

/**
 * Tests options list argument for views.
 *
 * @see \Drupal\options\Plugin\views\argument\NumberListField.
 * @group views
 */
class OptionsListArgumentTest extends OptionsTestBase {

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_options_list_argument_numeric', 'test_options_list_argument_string'];

  /**
   * Tests the options field argument.
   */
  public function testViewsTestOptionsListArgument() {

    $view = Views::getView('test_options_list_argument_numeric');
    $this->executeView($view, [0]);

    // The expected resultset is wrong. The view sorts the nodes by nid and DESC.
    $resultset = [
      ['nid' => $this->nodes[1]->nid->value],
      ['nid' => $this->nodes[0]->nid->value],
    ];

    $column_map = ['nid' => 'nid'];
    $this->assertIdenticalResultset($view, $resultset, $column_map);

    // I have no idea this test will work?! Was the idea to use the view
    // "test_options_list_filter". There are no values set on the current view
    // that hold "man" and/or "woman".
    $view = Views::getView('test_options_list_argument_string');
    $this->executeView($view, $this->fieldValues);

    // The expected resultset is wrong. The view sorts the nodes by nid and DESC.
    $resultset = [
      ['nid' => $this->nodes[1]->nid->value],
      ['nid' => $this->nodes[0]->nid->value],
    ];

    $column_map = ['nid' => 'nid'];
    $this->assertIdenticalResultset($view, $resultset, $column_map);
  }

}
