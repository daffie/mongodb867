<?php

namespace Drupal\Core\Entity\Query\Sql;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;

/**
 * Provides a trait for the get table mapping method.
 */
trait TableMappingTrait {

  /**
   * Gets the schema for the given table.
   *
   * @param string $table
   *   The table name.
   *
   * @return array|false
   *   An associative array of table field mapping for the given table, keyed by
   *   columns name and values are just incrementing integers. If the table
   *   mapping is not available, FALSE is returned.
   */
  protected function getTableMapping($table, $entity_type_id) {
    $storage = $this->entityManager->getStorage($entity_type_id);
    if ($storage instanceof SqlEntityStorageInterface) {
      $mapping = $storage->getTableMapping()->getAllColumns($table);
    }
    else {
      return FALSE;
    }
    return array_flip($mapping);
  }

}
