<?php

namespace Drupal\Core\KeyValueStore;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Database\Query\Merge;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Defines a default key/value store implementation.
 *
 * This is Drupal's default key/value store implementation. It uses the database
 * to store key/value data.
 */
class DatabaseStorage extends StorageBase {

  use DependencySerializationTrait;

  /**
   * The serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The name of the SQL table to use.
   *
   * @var string
   */
  protected $table;

  /**
   * Overrides Drupal\Core\KeyValueStore\StorageBase::__construct().
   *
   * @param string $collection
   *   The name of the collection holding key and value pairs.
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   *   The serialization class to use.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to use.
   * @param string $table
   *   The name of the SQL table to use, defaults to key_value.
   */
  public function __construct($collection, SerializationInterface $serializer, Connection $connection, $table = 'key_value') {
    parent::__construct($collection);
    $this->serializer = $serializer;
    $this->connection = $connection;
    $this->table = $table;
  }

  /**
   * {@inheritdoc}
   */
  public function has($key) {
    try {
      return (bool) $this->connection->query('SELECT 1 FROM {' . $this->connection->escapeTable($this->table) . '} WHERE collection = :collection AND name = :key', [
        ':collection' => $this->collection,
        ':key' => $key,
      ])->fetchField();
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(array $keys) {
    $values = [];
    try {
      $result = $this->connection->query('SELECT name, value FROM {' . $this->connection->escapeTable($this->table) . '} WHERE name IN ( :keys[] ) AND collection = :collection', [':keys[]' => $keys, ':collection' => $this->collection])->fetchAllAssoc('name');
      foreach ($keys as $key) {
        if (isset($result[$key])) {
          $values[$key] = $this->serializer->decode($result[$key]->value);
        }
      }
    }
    catch (\Exception $e) {
      // @todo: Perhaps if the database is never going to be available,
      // key/value requests should return FALSE in order to allow exception
      // handling to occur but for now, keep it an array, always.
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getAll() {
    try {
      $result = $this->connection->query('SELECT name, value FROM {' . $this->connection->escapeTable($this->table) . '} WHERE collection = :collection', [':collection' => $this->collection]);
    }
    catch (\Exception $e) {
      $result = [];
    }

    $values = [];
    foreach ($result as $item) {
      if ($item) {
        $values[$item->name] = $this->serializer->decode($item->value);
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    $try_again = FALSE;
    try {
      // The key_value table might not yet exist.
      $this->connection->merge($this->table)
        ->keys([
          'name' => $key,
          'collection' => $this->collection,
        ])
        ->fields(['value' => $this->serializer->encode($value)])
        ->execute();
    }
    catch (\Exception $e) {
      // If there was an exception, try to create the key_value table.
      if (!$try_again = $this->ensureTableExists()) {
        // If the exception happened for other reason than the missing key_value
        // table, propagate the exception.
        throw $e;
      }
    }
    // Now that the key_value table has been created, try again if necessary.
    if ($try_again) {
      $this->connection->merge($this->table)
        ->keys([
          'name' => $key,
          'collection' => $this->collection,
        ])
        ->fields(['value' => $this->serializer->encode($value)])
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setIfNotExists($key, $value) {
    $try_again = FALSE;
    try {
      // The key_value table might not yet exist.
      $result = $this->connection->merge($this->table)
        ->insertFields([
          'collection' => $this->collection,
          'name' => $key,
          'value' => $this->serializer->encode($value),
        ])
        ->condition('collection', $this->collection)
        ->condition('name', $key)
        ->execute();
      return $result == Merge::STATUS_INSERT;
    }
    catch (\Exception $e) {
      // If there was an exception, try to create the key_value table.
      if (!$try_again = $this->ensureTableExists()) {
        // If the exception happened for other reason than the missing key_value
        // table, propagate the exception.
        throw $e;
      }
    }
    // Now that the key_value table has been created, try again if necessary.
    if ($try_again) {
      $result = $this->connection->merge($this->table)
        ->insertFields([
          'collection' => $this->collection,
          'name' => $key,
          'value' => $this->serializer->encode($value),
        ])
        ->condition('collection', $this->collection)
        ->condition('name', $key)
        ->execute();
      return $result == Merge::STATUS_INSERT;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function rename($key, $new_key) {
    try {
      $this->connection->update($this->table)
        ->fields(['name' => $new_key])
        ->condition('collection', $this->collection)
        ->condition('name', $key)
        ->execute();
    }
    catch (\Exception $e) {
      // If there was an exception, try to create the key_value table.
      $this->ensureTableExists();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $keys) {
    try {
      // Delete in chunks when a large array is passed.
      while ($keys) {
        $this->connection->delete($this->table)
          ->condition('name', array_splice($keys, 0, 1000), 'IN')
          ->condition('collection', $this->collection)
          ->execute();
      }
    }
    catch (\Exception $e) {
      // If there was an exception, try to create the key_value table.
      $this->ensureTableExists();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    try {
      $this->connection->delete($this->table)
        ->condition('collection', $this->collection)
        ->execute();
    }
    catch (\Exception $e) {
      // If there was an exception, try to create the key_value table.
      $this->ensureTableExists();
    }
  }

  /**
   * Check if the key_value table exists and create it if not.
   */
  protected function ensureTableExists() {
    try {
      $database_schema = $this->connection->schema();
      if (!$database_schema->tableExists($this->table)) {
        $schema_definition = $this->schemaDefinition();
        $database_schema->createTable($this->table, $schema_definition);
        return TRUE;
      }
    }
    // If another process has already created the key_value table, attempting to
    // recreate it will throw an exception. In this case just catch the
    // exception and do nothing.
    catch (SchemaObjectExistsException $e) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Defines the schema for the key_value table.
   */
  public function schemaDefinition() {
    $schema = [
      'description' => 'Generic key-value storage table. See the state system for an example.',
      'fields' => [
        'collection' => [
          'description' => 'A named collection of key and value pairs.',
          'type' => 'varchar_ascii',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
        ],
        'name' => [
          'description' => 'The key of the key-value pair. As KEY is a SQL reserved keyword, name was chosen instead.',
          'type' => 'varchar_ascii',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
        ],
        'value' => [
          'description' => 'The value.',
          'type' => 'blob',
          'not null' => TRUE,
          'size' => 'big',
        ],
      ],
      'primary key' => ['collection', 'name'],
    ];
    return $schema;
  }

}
